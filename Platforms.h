#pragma once

#include "StaticWorld.h"

class Platforms : public StaticWorld{
private:
	GLuint texture;
public:
	//constructors
	Platforms();

	Platforms(Vec2 ipos, Vec2 isize, GLuint texture);

	Platforms(float x, float y, float width, float height, GLuint texture);

};