//
// This is derived from the Graphics 1 OpenGL template
//

#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls
#include "MainMenu.h"
#include "LoadPNG.h"
#include "Button.h"

#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE

std::vector<Button*> buttons;

MainMenu::MainMenu(OpenGLApplication *app)
	: Activity(app)		// Call the super constructor
{}


void MainMenu::initialise()
{
	// Initialise the activity; called at application start up
	LoadPNG imageloader = LoadPNG();
	backgroundTex = imageloader.loadPNG("Background_textures/Full-background.png");

	titleTex = imageloader.loadPNG("Background_textures/Title.png");

	Button* level = new Button(0, 4, 20, 5, "Background_textures/Level.png", "Background_textures/Level-select.png",1);
	buttons.push_back(level);

	Button* controls = new Button(0, -2, 20, 5, "Background_textures/Controls.png", "Background_textures/Controls-select.png",2);
	buttons.push_back(controls);

	Button* exit = new Button(0, -8, 20, 5, "Background_textures/exit.png", "Background_textures/exit-select.png",3);
	buttons.push_back(exit);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}


void MainMenu::shutdown()
{
	// Shutdown the activity; called at application finish

	// Delete the texture
	glDeleteTextures(1, &backgroundTex);
	glDeleteTextures(2, &titleTex);
	buttons.clear();
}


/*
*
* ACTIVITY METHODS
* Put your application/game code here
*
*/
void MainMenu::onSwitchIn()
{
	// Activity switched in

	glClearColor(0.0,0.0,0.0,1.0);						//sets the clear colour to black
}

void MainMenu::finish(){
	shutdown();
}


void MainMenu::onReshape(int w, int h)
{
	glViewport(0, 0, w, h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect, -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();
}

void MainMenu::draw()
{
	// OpenGL render calls go in this method
	double aspect = app->getAspectRatio();
	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, backgroundTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, titleTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-10, 5);
			glTexCoord2f(0, 1); glVertex2f(-10, 15);
			glTexCoord2f(1, 1); glVertex2f(10, 15);
			glTexCoord2f(1, 0); glVertex2f(10, 5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	// Disable 2D texturing
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	for each(Button* b in buttons){
		b->draw();
	}

	glFlush();
	
}

void MainMenu::update(double deltaT, double prevDeltaT){
	for each(Button* b in buttons){
		b->setCollidables();
		b->update(deltaT, prevDeltaT);
	}
}

void MainMenu::onMouseDown(int button, float mouseX, float mouseY){	// Called when mouse button pressed

}
void MainMenu::onMouseUp(int button, float mouseX, float mouseY){		// Called when mouse button released

	for (int i = 0; i < buttons.size(); i++){
		Button* b = buttons.at(i);
		if (b->isSelected() && b->getID() == 1 && button == 0){
			app->swicthActivity(app->levelSelect);
		}
		if (b->isSelected() && b->getID() == 2 && button == 0){
			app->swicthActivity(app->controls);
		}
		if (b->isSelected() && b->getID() == 3 && button == 0){
			app->finish();
		}
	}
}
void MainMenu::onMouseMove(float mouseX, float mouseY){				// Called when mouse moved
	double aspect = app->getAspectRatio();
	double oriAspect = app->getOriAspectRatio();
	float x = ((mouseX - app->getScreenWidth() / 2) / (VIEW_SIZE*0.5*oriAspect*(oriAspect / aspect) * ((double)app->getScreenWidth() / (double)app->getOriScreenWidth())));
	float y = ((app->getScreenHeight() / 2 - mouseY) / (VIEW_SIZE*0.5*oriAspect* ((double)app->getScreenHeight() / (double)app->getOriScreenHeight())));
	
	for each(Button* b in buttons){
		b->checkForCollisions(x, y);
	}
}
void MainMenu::onKeyDown(int key){										// Called when key pressed


}									
void MainMenu::onKeyUp(int key){										// Called when key released
}		
