#pragma once

#include <windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include "GameObject.h"
#include "Traps.h"
#include "Platforms.h"
#include "MovingPlatforms.h"
#include "BoundingBox.h"
#include "BoundingCircle.h"



#include <iostream>
using namespace std;


class Player : public GameObject{
private:
	Vec2 vel, ori;
	bool jump;
	vector <GLuint> moving_textures;
	vector <GLuint> jumping_textures;
	int direction;
	bool isDead;
	double frames;
public:
	//constructors
	Player(Vec2 ipos,Vec2 ivel, Vec2 isize);

	Player(float x, float y, float width, float height);
	//getters & setters for vel
	Vec2 getVel();

	void setVelX(float x);
	void setVelY(float y);
	//getter & setter for direction
	int getDirection();
	void setDirection(int newDir);
	//draw method
	void draw();
	//update method
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//add and getting textures
	vector<GLuint> getMovingTextures();
	void addMovingTexture(GLuint texture);

	vector<GLuint> getJumpingTextures();
	void addJumpingTexture(GLuint texture);

	void setTextures();
	//check collisions 
	bool checkForCollisions(Platforms* g, double deltaT, double prevDeltaT);
	bool checkForCollisions(MovingPlatforms* g, double deltaT, double preDeltaT);
	bool checkForCollisions(Traps* g, double deltaT, double prevDeltaT);


	void setJump(bool jump);
	bool getJump();

	void setDead();
	bool checkLife();
};
