#pragma once

#include "DeathObjects.h"

class Traps : public DeathObjects{
private:
	char* textureFile;
public:
	//constructors
	Traps(Vec2 ipos, Vec2 isize);

	Traps(float x, float y, float width, float height);
	//set textures
	void setTextures(char* file);

};