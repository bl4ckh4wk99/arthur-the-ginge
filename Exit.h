#pragma once

#include "StaticWorld.h"
#include "Player.h"


class Exit : public StaticWorld{
private:
	float frames;
	bool activated;
public:
	//constructors
	Exit(Vec2 pos, Vec2 size);

	Exit(float x, float y, float width, float height);
	//draw method
	void draw();
	//update method
	void update(double deltaT, double prevDeltaT);
	//check collisions with player
	bool checkForCollisions(Player* p, double deltaT, double prevDeltaT);
	//set textures
	void setTextures();
};