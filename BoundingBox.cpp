#pragma once

#include "BoundingBox.h"
//default constructor for bounding box
BoundingBox::BoundingBox(){}
//constructor for bounding box using vec2s
BoundingBox::BoundingBox(Vec2 pos, Vec2 size){
	this->pos = pos;
	this->size = size;
}
//operator overload to set this bounding box equal to another
BoundingBox BoundingBox::operator = (const BoundingBox &v){
	this->pos = v.pos;
	this->size = v.size;
	return *this;
}

//draw the bounding box
void BoundingBox::draw(){
	//push matrix
	glPushMatrix();
	//colour the box (debugging)
	glColor3f(0.0f, 1.0f, 0.0f);
	//draw the verticies ofr the box using line loop
	glBegin(GL_LINE_LOOP);
		glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
		glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
		glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
	glEnd();
	//pop the matrix
	glPopMatrix();

}
//update the bounding box using a vec2 of position
void BoundingBox::update(Vec2 attachedNewPos){

	this->pos.setX(attachedNewPos.getX());
	this->pos.setY(attachedNewPos.getY());
}