#include "Follower.h"

Follower::Follower(Vec2 ipos, Vec2 isize, int ID){
	this->pos = ipos;
	this->size = isize;
	this->ID = ID;
	this->frames = 0;
	this->activated = false;
}

Follower::Follower(float x, float y, float width, float height, int ID){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(width, height);
	this->pos = pos;
	this->size = size;
	this->ID = ID;
	this->frames = 0;
	this->activated = false;
}

//getter for direction
int Follower::getDirection(){
	return this->direction;
}

//setter for direction
void Follower::setDirection(int newDir){
	this->direction = newDir;
}

void Follower::draw(){
	//push the matrix,
	glPushMatrix();
		//set the colour
		//glColor3f(1.0f, 0.0f, 0.0f);
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the vertices using the position and size of the object
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(direction, 1.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(direction, 0.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glEnd();
	//pop the matrix
	glPopMatrix();

	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	//circle.draw();
}

void Follower::update(double deltaT, double prevDeltaT){}

void Follower::update(double deltaT, double prevDeltaT, Player* p){

	frames += (4 * deltaT);
	if (frames > idle_textures.size()-1)
		frames = 0;

	int it = (int)frames;
	this->currentTexture = this->idle_textures.at(it);

	if (!activated){
		if (p->getPos().getX() > this->pos.getX())
			this->direction = 1;
		else
			this->direction = -1;
	}
	else{
		Vec2 playerPos = p->getPos();

		float xDiff = (playerPos.getX() -p->getDirection() * (3*ID)) - this->pos.getX();
		if (xDiff > 0)
			direction = 1;
		else
			direction = -1;
		float yDiff = playerPos.getY() - this->pos.getY();

		this->pos.setX(pos.getX() + (xDiff * deltaT));
		this->pos.setY(pos.getY() + (yDiff * deltaT));
		cout << pos.getX() << "," << pos.getY() << endl;
	}

}


void Follower::setCollidables(){
	//create a bounding circle
	float radius = pow((this->size.getX() / 2), 2) + pow((this->size.getY() / 2), 2);
	radius = sqrt(radius);
	BoundingCircle circle = BoundingCircle(this->pos, radius);
	this->circle = circle;
}
bool Follower::checkForCollisions(Player* g, double deltaT, double prevDeltaT){
	//bool variable to store results
	bool collision = false;
	//bool variable for if the circles are colliding
	bool circleTouch = false;
	//get the 2 bounding circles positions and radius
	float x1 = this->circle.getPos().getX();
	float x2 = g->getCircle().getPos().getX();
	float y1 = this->circle.getPos().getY();
	float y2 = g->getCircle().getPos().getY();
	float r1 = this->circle.getSize().getX();
	float r2 = g->getCircle().getSize().getX();

	//Check circles first
	if ((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) < (r1 + r2)*(r1 + r2)){
		circleTouch = true;
	}

	//if circles collided set active
	if (circleTouch){
		setActive();
		return true;
	}
	return false;
}

void Follower::setTextures(){
	//idle
	GLuint idle1 = loadPNG("Follower_textures/frame-1.png");
	GLuint idle2 = loadPNG("Follower_textures/frame-2.png");
	GLuint idle3 = loadPNG("Follower_textures/frame-3.png");
	GLuint idle4 = loadPNG("Follower_textures/frame-4.png");

	this->addIdleTexture(idle1);
	this->addIdleTexture(idle2);
	this->addIdleTexture(idle3);
	this->addIdleTexture(idle4);


}

bool Follower::isActive(){
	return this->activated;
}
void Follower::setActive(){
	this->activated = true;
}