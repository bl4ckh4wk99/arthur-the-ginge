#pragma once

#include "GameObject.h"

class DeathObjects : public GameObject{
public:
	//constructors
	DeathObjects();
	//draw method
	void draw();
	//update method
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//abstract method for setting textures
	virtual void setTextures();
};