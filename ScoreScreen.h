//
// This is derived from the Graphics 1 OpenGL template
//

// Ensure that this file can only be included once
#pragma once

#include "Activity.h"
#include "LoadPNG.h"
#include "Button.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

class ScoreScreen : public Activity
{
private:
	int score;
	bool newHighscore;
	GLuint background, tensTho, tho, hun, tens, units, highscoreTex,  scoreTex;
	vector<GLuint> numbers, highscoreTexs;
	vector<Button*> buttons;
	double frames;
public:

	ScoreScreen(OpenGLApplication *app);

	virtual void initialise();											// Called on application start up
	virtual void shutdown();											// Called on application shut down
	virtual void finish();												// Called when the activity is finished and needs to be rebuilt from scratch

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onSwitchOut();
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void update(double deltaT, double prevDeltaT);				// Update the application;
	virtual void draw();												// Render function

	// Event handler methods; these methods are called in response to user actions:
	void onKeyDown(int key);									// Called when key pressed
	void onKeyUp(int key);										// Called when key released
	void onMouseDown(int button, float mouseX, float mouseY);		// Called when mouse button pressed
	void onMouseUp(int button, float mouseX, float mouseY);			// Called when mouse button released
	void onMouseMove(float mouseX, float mouseY);					// Called when mouse moved


};