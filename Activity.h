//
// This is derived from the Graphics 1 OpenGL template
//

#pragma once

#include <Windows.h>
#include "glew.h"
#include <gl/gl.h>			// Header File For The OpenGL32 Library
#include "OpenGLApplication.h"
#include "Inputs.h"
#include <iostream>

using namespace std;



class Activity
{
protected:
	OpenGLApplication *app;
	Inputs* input;

public:
	Activity(OpenGLApplication *app);

	/*
	*
	* ACTIVITY METHODS
	*
	*/
	virtual void initialise();											// Initialise the activity when required
	virtual void finish();												// Called when the activity is finished and needs to be rebuilt from scratch

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onSwitchOut();											// Activity switch out; called when the activity changes and this one switches out

	virtual void shutdown();											// Shutdown the activity at application close

	virtual void onReshape(int width, int height);						// Called when the window is resized
	virtual void update(double deltaT, double prevDeltaT);				// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2
																		// deltaT is the time elapsed from frame F-1 to frame F, prevDeltaT is the time elapsed from F-2 to F-1
	virtual void draw();												// draw function

	// Event handler methods; these methods are called in response to user actions:
	virtual void onMouseDown(int button, float mouseX, float mouseY) = 0;	// Called when mouse button pressed
	virtual void onMouseUp(int button, float mouseX, float mouseY) = 0;		// Called when mouse button released
	virtual void onMouseMove(float mouseX, float mouseY) = 0;				// Called when mouse moved
	virtual void onKeyDown(int key) = 0;									// Called when key pressed
	virtual void onKeyUp(int key) = 0;										// Called when key released
};