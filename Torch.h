#pragma once

#include <windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include <string>
#include "GameObject.h"
#include "BoundingBox.h"
#include "BoundingCircle.h"

#include <iostream>
using namespace std;

class Torch : public GameObject{
private:
	float frames;
public:
	//constructors
	Torch(Vec2 pos);
	Torch(float x, float y);
	//draw method
	void draw();
	//update method
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//set textures
	void setTextures();

};