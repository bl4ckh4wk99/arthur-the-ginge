#pragma once
#include "Vec2.h"

class CollidableObjects{
	protected:
		//position and size
		Vec2 pos, size;
	public:
		//constructors
		CollidableObjects();
		CollidableObjects(Vec2 ipos, Vec2 isize);
		CollidableObjects(float x, float y, float width, float height);
		//getters & setters
		Vec2 getPos();
		void setPos(float x, float y);

		Vec2 getSize();
		void setSize(float width, float height);
		//draw and update abstract methods
		virtual void draw() = 0;//{ cout << "inside stupid draw function" << endl; };
		virtual void update(Vec2 attachedNewPos) = 0;
	};

