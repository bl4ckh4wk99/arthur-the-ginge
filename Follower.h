#pragma once

#include "GameObject.h"
#include "Player.h"

class Follower : public GameObject{
private:
	int ID;
	int direction;
	bool activated;
	double frames;

public:
	//constructors
	Follower(Vec2 ipos, Vec2 isize, int ID);

	Follower(float x, float y, float width, float height, int ID);
	//getter and setter for direction
	int getDirection();
	void setDirection(int newDir);
	//draw method
	void draw();
	//update methods
	void update(double deltaT, double prevDeltaT, Player* p);
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//check collisions
	bool checkForCollisions(Player* g, double deltaT, double prevDeltaT);
	//set textures
	void setTextures();
	//getter and setter for activated
	bool isActive();
	void setActive();
};