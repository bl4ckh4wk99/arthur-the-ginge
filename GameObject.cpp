#include "GameObject.h"

//default constructor for a game object
	GameObject::GameObject(){}
	//constructor for a game object using vec2s
	GameObject::GameObject(Vec2 ipos, Vec2 isize){
		this->pos = ipos;
		this->size = isize;
	}
	//constructor for a game object using floats
	GameObject::GameObject(float x, float y, float width, float height){
		Vec2 pos = Vec2(x,y);
		Vec2 size = Vec2(width,height);
		this->pos = pos;
		this->size = size;
	}	

	//getter for pos
	Vec2 GameObject::getPos(){
		return this->pos;
	}
	//setter for pos
	void GameObject::setPos(float x, float y){
		Vec2 newPos = Vec2(x,y);
		this->pos = newPos;
	}
	//getter for size
	Vec2 GameObject::getSize(){
		return this->size;
	}
	//setter for size
	void GameObject::setSize(float width, float height){
		Vec2 newSize = Vec2(width, height);
		this->size = newSize;
	}
	//getter for bounding circle
	BoundingCircle GameObject::getCircle(){
		return this->circle;
	}
	//setter for bounding circle
	void GameObject::setCircle(BoundingCircle circle){
		this->circle = circle;
	}
	//setter for bounding box
	void GameObject::setBox(BoundingBox box){
		this->box = box;
	}
	//setter for bounding box
	BoundingBox GameObject::getBox(){
		return this->box;
	}

	vector<GLuint> GameObject::getIdleTextures(){
		return this->idle_textures;
	}
	void GameObject::addIdleTexture(GLuint texture){
		this->idle_textures.push_back(texture);
	}

	GLuint GameObject::loadPNG(const char * filename)
	{	
		GLuint retVal;
		//following to be placed in the init function
		nv::Image img;
		// Return true on success
		if (img.loadImageFromFile(filename))
		{
			glGenTextures(1, &retVal);
			glBindTexture(GL_TEXTURE_2D, retVal);
			//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(),img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		}
		else
			MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);


		return retVal;
	}


