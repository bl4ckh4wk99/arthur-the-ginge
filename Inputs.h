//
//This is derived from the Graphics 1 Open GL
//

#pragma once

#include "Vec2.h"

class Inputs{
private:
	bool keys[256];
	bool leftPressed;
	bool rightPressed;
	Vec2 mousePos;

public:
	//constructor
	Inputs();


	// Getter methods; use these to poll the state of the mouse and keyboard
	bool isMouseButtonPressed(int button) const;			// Determine if a button is pressed; 0 = LEFT, 2 = RIGHT
	Vec2 getMousePos() const;
	bool isKeyPressed(int key) const;						// Determine if a key is pressed



	// These methods are called by Win32 code to inform the InputState of user actions
	void handleMouseDown(int button, float mouseX, float mouseY);
	void handleMouseUp(int button, float mouseX, float mouseY);
	void handleMouseMove(float mouseX, float mouseY);
	void handleKeyDown(int key);
	void handleKeyUp(int key);

};