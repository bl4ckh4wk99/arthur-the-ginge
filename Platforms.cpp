#include "Platforms.h"

Platforms::Platforms(){}

//constructor for a non moving static world object using vec2s
Platforms::Platforms(Vec2 ipos, Vec2 isize, GLuint texture){
	this->pos = ipos;
	this->size = isize;
	this->currentTexture = texture;
}
//constructor for a non moving static world object using floats
Platforms::Platforms(float x, float y, float width, float height, GLuint texture){
	Vec2 pos = Vec2(x,y);
	Vec2 size = Vec2(width,height);
	this->pos = pos;
	this->size = size;
	this->currentTexture = texture;
}