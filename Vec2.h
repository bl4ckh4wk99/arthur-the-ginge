#pragma once

class Vec2{
private:
	float x, y;

public:
	//constructors
	Vec2();

	Vec2(float ix, float iy);
	//operator overloads
	Vec2 operator = (const Vec2 &v);

	Vec2& operator += (const Vec2 &v);

	Vec2& operator + (const Vec2 &v);

	Vec2& operator - (const Vec2 &v);

	Vec2& operator -= (const Vec2 &v);
	//getters and setters
	float getX();
	void setX(float x);

	float getY();
	void setY(float y);
};