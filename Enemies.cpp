#include "Enemies.h"

//constructor for a non moving static world object using vec2s
Enemies::Enemies(Vec2 ipos, Vec2 isize, int endX){
	this->pos = ipos;
	this->size = isize;
	this->frames = 0.0f;
	this->isDead = false;
	this->chasing = false;
	this->startX = ipos.getX();
	normalVel = 30;
	chaseVel = 40;
	if (endX > startX){
		this->vel.setX(30.0);
		this->dir = 1;
		this->endX = endX;
	}
	else{
		this->vel.setX(-30.0);
		this->dir = -1;
		this->endX = endX;
	}
}

//constructor for a non moving static world object using floats
Enemies::Enemies(float x, float y, float width, float height, int endX){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(width, height);
	this->pos = pos;
	this->size = size;
	this->frames = 0.0f;
	this->isDead = false;
	this->chasing = false;
	normalVel = 30;
	chaseVel = 40;
	if (endX > x){	
		this->startX = x;
		this->vel.setX(30.0);
		this->dir = 1;
		this->endX = endX;
	}
	else{
		this->startX = endX;
		this->vel.setX(-30.0);
		this->dir = -1;
		this->endX = x;
	}
}

//getter for velocity
Vec2 Enemies::getVel(){
	return this->vel;
}
//setter for velocity X value
void Enemies::setVelX(float x){
	this->vel.setX(x);
}
//setter for velocity Y value
void Enemies::setVelY(float y){
	this->vel.setY(y);
}

bool Enemies::checkIfChase(Player* p){
	Vec2 temp = p->getPos();
	//if player is in the height radius of the zombie
	if ((int)temp.getY() == (int)this->pos.getY()){
		//if the player is in the x radius of the zombie
		if (startX <= temp.getX() && temp.getX() <= endX){
			//chase the player
			this->chasing = true;
			return true;
		}
	}
	this->chasing = false;
	return false;
}

void Enemies::chase(Player* p, double deltaT, double prevDeltaT){
	if (this->pos.getX() < p->getPos().getX()){
		dir = 1;
	}
	else{
		dir = -1;
	}
	vel.setX(chaseVel * dir);
	//set new x and y poisitions dependant on the velocity
	pos.setX(pos.getX() + vel.getX() / 10 * deltaT);

	pos.setY(pos.getY() + vel.getY() / 10 * deltaT);

	//add gravity velocity in y
	this->vel.setY(this->getVel().getY() - (deltaT * 300));
	//update its collidables
	box.update(this->pos);
	circle.update(this->pos);
}

//draw method
void Enemies::draw(){
	if (!checkDeath()){
		//push the matrix
		glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(-dir, 1.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(-dir, 0.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glEnd();
		//pop the matrix
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		//circle.draw();
		//box.draw();
	}
}

//update method
void Enemies::update(double deltaT, double prevDeltaT){
	frames += (2 * deltaT);
	int it = (int)frames;
	if (it >= idle_textures.size()){
		it = 0;
		frames = 0;
	}
	currentTexture = idle_textures.at(it);
	//if not chasing the player
	if (!chasing){
		if (this->pos.getX() <= startX){
			dir = 1;
			vel.setX(normalVel);
		}
		if (this->pos.getX() >= endX){
			dir = -1;
			vel.setX(-normalVel);
		}
		//set new x and y poisitions dependant on the velocity
		pos.setX(pos.getX() + vel.getX() / 10 * deltaT);

		pos.setY(pos.getY() + vel.getY() / 10 * deltaT);

		//add gravity velocity in y
		this->vel.setY(this->getVel().getY() - (deltaT * 300));
		//update its collidables
		box.update(this->pos);
		circle.update(this->pos);
	}
}

//method to check for collisions with a given gameobject
bool Enemies::checkForCollisions(GameObject* g, double deltaT, double prevDeltaT){
	//bool variable to store results
	bool collision = false;
	//bool variable for if the circles are colliding
	bool circleTouch = false;
	//get the 2 bounding circles positions and radius
	float x1 = this->circle.getPos().getX();
	float x2 = g->getCircle().getPos().getX();
	float y1 = this->circle.getPos().getY();
	float y2 = g->getCircle().getPos().getY();
	float r1 = this->circle.getSize().getX();
	float r2 = g->getCircle().getSize().getX();

	//Check circles first
	if ((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) < (r1 + r2)*(r1 + r2)){
		circleTouch = true;
	}

	//if circles collided do more thorough checking
	if (circleTouch){
		//get the 2 bounding boxes verticies 
		float xMinA = (this->box.getPos().getX()/*+ this->vel.getX()*/) - this->box.getSize().getX() / 2;
		float xMaxB = g->getBox().getPos().getX() + g->getBox().getSize().getX() / 2;
		float xMaxA = (this->box.getPos().getX()/*+ this->vel.getX()*/) + this->box.getSize().getX() / 2;
		float xMinB = g->getBox().getPos().getX() - g->getBox().getSize().getX() / 2;
		float yMinA = (this->box.getPos().getY()/*+ this->vel.getY()*/) - this->box.getSize().getY() / 2;
		float yMaxB = g->getBox().getPos().getY() + g->getBox().getSize().getY() / 2;
		float yMaxA = (this->box.getPos().getY()/*+ this->vel.getY()*/) + this->box.getSize().getY() / 2;
		float yMinB = g->getBox().getPos().getY() - g->getBox().getSize().getY() / 2;

		//bool variables for each side
		bool checkLeft = false;
		bool checkRight = false;
		bool checkBottom = false;
		bool checkTop = false;
		//response variables to know how much to respond
		float leftResponse = xMaxB - xMinA;
		float rightResponse = xMaxA - xMinB;
		float topResponse = yMaxA - yMinB;
		float bottomResponse = yMaxB - yMinA;

		//check which side is being collided
		if (xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		if (xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		if (yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		if (yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		// check left and if true respond accordingly
		if (checkLeft && checkBottom && checkTop || checkLeft && checkBottom && (leftResponse < bottomResponse)
			|| checkLeft && checkTop && (leftResponse < topResponse)){
			this->pos.setX(this->getPos().getX() + (leftResponse + 0.1));
			this->dir = 1;
			//make velocity dependant on direction
			this->vel.setX(-this->vel.getX());
			return true;
		}
		// check right and if true respond accordingly
		if (checkRight && checkBottom && checkTop || checkRight && checkBottom && (rightResponse < bottomResponse)
			|| checkRight && checkTop && (rightResponse < topResponse)){
			this->pos.setX(this->getPos().getX() + (rightResponse - 0.1));
			this->dir = -1;
			//make velocity dependant on direction
			this->vel.setX(-this->vel.getX());
			return true;
		}
		//check bottom and if true respond accordingly
		if (checkBottom && checkRight && (bottomResponse < rightResponse) || checkBottom && checkLeft && (bottomResponse < leftResponse)
			|| checkBottom && checkRight && checkLeft){
			this->pos.setY(this->getPos().getY() + bottomResponse);
			this->setVelY(-this->getVel().getY() * 0.3);
			return true;
		}
		//check top and if true respond accordingly
		if (checkTop && checkRight && (topResponse < rightResponse) || checkTop && checkLeft && (topResponse < bottomResponse)
			|| checkTop && checkRight && checkLeft){
			this->setVelY(-5.0f);
			return true;
		}
	}

	//return the result boolean
	return collision;

}

//method to check for collisions with a given death object
bool Enemies::checkForCollisions(Player* p, double deltaT, double prevDeltaT){
	//bool variable to store results
	bool collision = false;
	//bool variable for if the circles are colliding
	bool circleTouch = false;
	//get the 2 bounding circles positions and radius
	float x1 = this->circle.getPos().getX();
	float x2 = p->getCircle().getPos().getX();
	float y1 = this->circle.getPos().getY();
	float y2 = p->getCircle().getPos().getY();
	float r1 = this->circle.getSize().getX();
	float r2 = p->getCircle().getSize().getX();

	//Check circles first
	if ((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) < (r1 + r2)*(r1 + r2)){
		circleTouch = true;
	}

	//if circles collided do more thorough checking
	if (circleTouch){
		//get the 2 bounding boxes verticies 
		float xMinA = (this->box.getPos().getX()/*+ this->vel.getX()*/) - this->box.getSize().getX() / 2;
		float xMaxB = p->getBox().getPos().getX() + p->getBox().getSize().getX() / 2;
		float xMaxA = (this->box.getPos().getX()/*+ this->vel.getX()*/) + this->box.getSize().getX() / 2;
		float xMinB = p->getBox().getPos().getX() - p->getBox().getSize().getX() / 2;
		float yMinA = (this->box.getPos().getY()/*+ this->vel.getY()*/) - this->box.getSize().getY() / 2;
		float yMaxB = p->getBox().getPos().getY() + p->getBox().getSize().getY() / 2;
		float yMaxA = (this->box.getPos().getY()/*+ this->vel.getY()*/) + this->box.getSize().getY() / 2;
		float yMinB = p->getBox().getPos().getY() - p->getBox().getSize().getY() / 2;

		//bool variables for each side
		bool checkLeft = false;
		bool checkRight = false;
		bool checkBottom = false;
		bool checkTop = false;
		//response variables to know how much to respond
		float leftResponse = xMaxB - xMinA;
		float rightResponse = xMaxA - xMinB;
		float topResponse = yMaxA - yMinB;
		float bottomResponse = yMaxB - yMinA;

		//check which side is being collided
		if (xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
		if (xMaxA >= xMinB && xMaxA <= xMaxB)
			checkRight = true;
		if (yMinA <= yMaxB && yMinA >= yMinB)
			checkBottom = true;
		if (yMaxA >= yMinB && yMaxA <= yMaxB)
			checkTop = true;
		// check left and if true respond accordingly
		if (checkLeft && checkBottom && checkTop || checkLeft && checkBottom && (leftResponse < bottomResponse)
			|| checkLeft && checkTop && (leftResponse < topResponse)){
			this->pos.setX(this->getPos().getX() + (leftResponse + 0.1));
			this->getVel().setX(this->getVel().getX() - 1 * deltaT);
			this->dir = 1;
			//make velocity dependant on direction
			this->vel.setX(-this->vel.getX());
			p->setDead();
			return true;
		}
		// check right and if true respond accordingly
		if (checkRight && checkBottom && checkTop || checkRight && checkBottom && (rightResponse < bottomResponse)
			|| checkRight && checkTop && (rightResponse < topResponse)){
			this->pos.setX(this->getPos().getX() + (rightResponse - 0.1));
			this->getVel().setX(this->getVel().getX() - 1 * deltaT);
			this->dir = -1;
			//make velocity dependant on direction
			this->vel.setX(-this->vel.getX());
			p->setDead();
			return true;
		}
		//check bottom and if true respond accordingly
		if (checkBottom && checkRight && (bottomResponse < rightResponse) || checkBottom && checkLeft && (bottomResponse < leftResponse)
			|| checkBottom && checkRight && checkLeft){
			this->pos.setY(this->getPos().getY() + bottomResponse);
			this->setVelY(-this->getVel().getY() * 0.3);
			this->setDead();
			return false;
		}
		//check top and if true respond accordingly
		if (checkTop && checkRight && (topResponse < rightResponse) || checkTop && checkLeft && (topResponse < bottomResponse)
			|| checkTop && checkRight && checkLeft){
			this->pos.setY(this->getPos().getY() - (topResponse + 0.2));
			this->setVelY(-5.0f);
			setDead();
			return false;
		}
	}

	//return the result boolean
	return collision;

}


void Enemies::setDead(){
	this->isDead = true;
}

bool Enemies::checkDeath(){
	return this->isDead;
}

void Enemies::setTextures(){
	GLuint idle1 = loadPNG("Enemy_textures/frame-1.png");
	GLuint idle2 = loadPNG("Enemy_textures/frame-2.png");

	idle_textures.push_back(idle1);
	idle_textures.push_back(idle2);
}