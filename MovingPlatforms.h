#pragma once

#include "Platforms.h"

class MovingPlatforms : public Platforms{
private:
	Vec2 vel;
	float startX;
	float endX;
	float startY;
	float endY;
	bool horizontal;
	GLuint hStart,hMid,hEnd, vStart, vMid, vEnd;
public:
	//constructors
	MovingPlatforms(Vec2 ipos, Vec2 isize, float iendX, float iendY);

	MovingPlatforms(float x, float y, float width, float height, float iendX, float iendY);
	//set texture method
	void setTextures();
	//draw methods
	void draw();

	void drawHSlider();
	void drawVSlider();
	//update method
	void update(double deltaT, double prevDeltaT);
	//getters & setters for vel
	Vec2 getVel();

	void setVelX(float x);
	void setVelY(float y);

};