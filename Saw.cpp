#include "Saw.h"

Saw::Saw(Vec2 ipos, float radius, float iendX, float iendY){
	this->pos = ipos;
	this->size = Vec2(radius, 0.0);
	if(iendX != 0){
		this->vel.setX(50.0);
		this->startX = ipos.getX();
		this->endX = iendX;
		horizontal = true;
		
	}
	else{
		this->vel.setY(50.0);
		this->startY = ipos.getY();
		this->endY = iendY;
		horizontal = false;
	}
	frames =0;
}

Saw::Saw(float x, float y, float radius, float iendX, float iendY){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(radius,0);
	this->pos = pos;
	this->size = size;
	if(iendX != 0){
		this->vel.setX(50.0);
		this->startX = x;
		this->endX = iendX;
		horizontal = true;
	}
	else{
		this->vel.setY(50.0);
		this->startY = y;
		this->endY = iendY;
		horizontal = false;
	}
	frames =0;
}

void Saw::setTextures(){
	currentTexture = loadPNG("Trap_textures/saw.png");
	this->addIdleTexture(currentTexture);
	currentTexture = loadPNG("Trap_textures/saw2.png");
	this->addIdleTexture(currentTexture);
	hStart = loadPNG("Trap_textures/hSlider1.png");
	hMid = loadPNG("Trap_textures/hSlider2.png");
	hEnd = loadPNG("Trap_textures/hSlider3.png");
	vStart = loadPNG("Trap_textures/vSlider3.png");
	vMid = loadPNG("Trap_textures/vSlider2.png");
	vEnd = loadPNG("Trap_textures/vSlider1.png");
}

//set collidables for this object
void Saw::setCollidables(){
	//create bounding circle
	float radius = this->size.getX();
	BoundingCircle circle = BoundingCircle(this->pos, radius);
	this->circle = circle;
}

void Saw::draw(){
	if(horizontal)
		drawHSlider();
	else
		drawVSlider();

	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - this->size.getX(), this->pos.getY() - this->size.getX());
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX(), this->pos.getY() + this->size.getX());
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + this->size.getX(), this->pos.getY() + this->size.getX());
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + this->size.getX(), this->pos.getY() - this->size.getX());
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	//circle.draw();
}

void Saw::drawHSlider(){
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, hStart);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(startX,this->pos.getY()-0.5);
			glTexCoord2f(0.0, 1.0); glVertex2f(startX, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 1.0); glVertex2f(startX + 5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 0.0); glVertex2f(startX + 5, this->pos.getY()-0.5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, hMid);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(startX + 5, this->pos.getY()-0.5);
			glTexCoord2f(0.0, 1.0); glVertex2f(startX + 5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 1.0); glVertex2f(endX - 5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 0.0); glVertex2f(endX - 5, this->pos.getY()-0.5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, hEnd);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(endX-5, this->pos.getY()-0.5);
			glTexCoord2f(0.0, 1.0); glVertex2f(endX-5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 1.0); glVertex2f(endX, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 0.0); glVertex2f(endX, this->pos.getY()-0.5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

}

void Saw::drawVSlider(){
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, vStart);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()-0.5, startY);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()-0.5, startY+5);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+0.5, startY+5);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+0.5, startY);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, vMid);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()-0.5, startY+5);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()-0.5, endY-5);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+0.5, endY-5);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+0.5, startY+5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, vEnd);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()-0.5, endY-5);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()-0.5, endY);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+0.5, endY);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+0.5, endY-5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

}

void Saw::update(double deltaT, double prevDeltaT){
	frames += (15*deltaT);
	int it = (int)frames;
	if (it >= idle_textures.size()){
		it = 0;
		frames = 0;
	}
	currentTexture = idle_textures.at(it);
	if(horizontal){
		if (pos.getX() < startX || pos.getX() > endX){
			vel.setX(-vel.getX());
		}
	}
	else{
		if (pos.getY() < startY || pos.getY() > endY){
			vel.setY(-vel.getY());
		}
	}

	//set new x and y poisitions dependant on the velocity
	pos.setX(pos.getX() + vel.getX() / 10 * deltaT);

	pos.setY(pos.getY() + vel.getY() / 10 * deltaT);

	//update its collidables
	circle.update(this->pos);
}

Vec2 Saw::getVel(){
	return this->vel;
}

void Saw::setVelX(float x){
	this->vel.setX(x);
}
void Saw::setVelY(float y){
	this->vel.setY(y);
}

bool Saw::checkForCollisions(Player* g, double deltaT, double prevDeltaT){
	//bool variable to store results
	bool collision = false;
	//bool variable for if the circles are colliding
	bool circleTouch = false;
	//get the 2 bounding circles positions and radius
	float x1 = this->circle.getPos().getX();
	float x2 = g->getCircle().getPos().getX();
	float y1 = this->circle.getPos().getY();
	float y2 = g->getCircle().getPos().getY();
	float r1 = this->circle.getSize().getX();
	float r2 = g->getCircle().getSize().getX();

	//Check circles first
	if ((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) < (r1 + r2)*(r1 + r2)){
		circleTouch = true;
	}

	if(circleTouch){

		Vec2 botL = Vec2(g->getBox().getPos().getX() - (g->getBox().getSize().getX() / 2), g->getBox().getPos().getY() - (g->getBox().getSize().getY() / 2));
		Vec2 topL = Vec2(g->getBox().getPos().getX() - (g->getBox().getSize().getX() / 2), g->getBox().getPos().getY() + (g->getBox().getSize().getY() / 2));
		Vec2 topR = Vec2(g->getBox().getPos().getX() + (g->getBox().getSize().getX() / 2), g->getBox().getPos().getY() + (g->getBox().getSize().getY() / 2));
		Vec2 botR = Vec2(g->getBox().getPos().getX() + (g->getBox().getSize().getX() / 2), g->getBox().getPos().getY() - (g->getBox().getSize().getY() / 2));

		if ((x1 - botL.getX())*(x1 - botL.getX()) + (y1 - botL.getY())*(y1 - botL.getY()) < (r1 * r1)){
			collision = true;
			g->setDead();
		}
		if ((x1 - topL.getX())*(x1 - topL.getX()) + (y1 - topL.getY())*(y1 - topL.getY()) < (r1 * r1)){
			collision = true;
			g->setDead();
		}
		if ((x1 - topR.getX())*(x1 - topR.getX()) + (y1 - topR.getY())*(y1 - topR.getY()) < (r1 * r1)){
			collision = true;
			g->setDead();
		}
		if ((x1 - botR.getX())*(x1 - botR.getX()) + (y1 - botR.getY())*(y1 - botR.getY()) < (r1 * r1)){
			collision = true;
			g->setDead();
		}
		if(collision)
			cout << "Collided" << endl;

	}
	return collision;
}