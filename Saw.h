#pragma once

#include "DeathObjects.h"
#include "Player.h"

class Saw : public DeathObjects{
private:
	Vec2 vel;
	float startX;
	float endX;
	float startY;
	float endY;
	bool horizontal;
	float frames;
	GLuint hStart,hMid,hEnd, vStart, vMid, vEnd;
public:
	//constrcutors
	Saw(Vec2 ipos, float radius, float iendX, float iendY);

	Saw(float x, float y, float radius, float iendX, float iendY);

	//set textures & collidables
	void setTextures();

	void setCollidables();

	//draw methods
	void draw();

	void drawHSlider();
	void drawVSlider();

	//update method
	void update(double deltaT, double prevDeltaT);

	//getters and setters for vel
	Vec2 getVel();

	void setVelX(float x);
	void setVelY(float y);

	//check collisions
	bool checkForCollisions(Player* g, double deltaT, double prevDeltaT);

};