//
//This is derived from the Graphics 1 OpenGL
//

// Ensure that this file can only be included once
#pragma once

#include "Inputs.h"
#include "Hud.h"

class Activity;

/*
OpenGLApplication delegates the application work to an Activity; see the Activity class, particularly the GameActivity class.

There is ONE activity that is currently running. 
*/
class OpenGLApplication
{
protected:
	Inputs input;																			// Input; maintains state of input devices (mouse, keyboard)
	int screenWidth, screenHeight, oriScreenWidth, oriScreenHeight;							// Screen size
	Activity *currentActivity, *previousActivity;											// The current activity; e.g. start screen, main game, end screen
	

private:
	bool finished;

public:
	//list of all the activities that are available
	Activity *mainMenu, *levelOne, *levelTwo, *death, *controls, *levelSelect, *scoreScreen, *time;
public:
	OpenGLApplication();


	// Getters for input state and screen size
	Inputs * getInputState();
	//const Inputs & getInputState() const;
	int getScreenWidth() const;
	int getScreenHeight() const;
	int getOriScreenWidth() const;
	int getOriScreenHeight() const;
	double getAspectRatio() const;
	double getOriAspectRatio() const;


	// Activity
	Activity* getCurrentActivity();					// returns the current activity
	void swicthActivity(Activity *a);				//Switches the current activity out and the given activity in
	void playerDied();								//Switches activity to the death screen
	void playerRespawn();							//Switches activity to the previous activity
	void timeRanOut();								//Switches Activity to out of time screen, ending & clearing the level to start a new

	void startLevel(Activity *a);					//Starts a new level, initialises it and the death screen
	void restartLevel();							//Reestarts the previous level with all objects in the original start posiiton
	void finishLevel(Hud *hud, int levelNum);		//called when the level is finished, finishes the currentActivity and passes the Hud to the level complete activity so that score can be calcilated
	void exitLevel();								//called when a level is quit after death and will take you to main menu


	// Initialise/shutdown application
	void initialise();
	void shutdown();
	
	// Finish
	void finish();						// Call to cause the application to close
	bool isApplicationFinished();		// Used by the main loop to see if we are done yet


	
	/*
	*
	* Methods called by Win32 code to inform our application that it must start up, update, render, reshape,
	* and of user actions; mouse down/up, mouse move, key down/up
	*
	* Mainly, they just pass on events to the current activity.
	*
	* You don't need to add to these methods.
	*
	*/
	void handleReshape(int width, int height);
	void handleUpdate(double deltaT, double prevDeltaT);
	void handleDraw();

	// Methods called by Win32 code to inform our application of user actions
	void handleMouseDown(int button, float mouseX, float mouseY);
	void handleMouseUp(int button, float mouseX, float mouseY);
	void handleMouseMove(float mouseX, float mouseY);
	void handleKeyDown(int key);
	void handleKeyUp(int key);
};
