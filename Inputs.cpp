

#include "Inputs.h"

Inputs::Inputs(){
	for(int i=0; i <256; i++){
		keys[i] = false;
	}
	leftPressed = false;
	rightPressed = false;
	mousePos = Vec2(0.0f,0.0f);

}

bool Inputs::isMouseButtonPressed(int button) const
{
	if(button == 0){
		return leftPressed;
	}
	else
		return rightPressed;
}

Vec2 Inputs::getMousePos() const
{
	return mousePos;
}

bool Inputs::isKeyPressed(int key) const
{
	return keys[key];
}


// These methods are called by Win32 code to inform the InputState of user actions
void Inputs::handleMouseDown(int button, float mouseX, float mouseY)
{
	if(button == 0){
		leftPressed = true;
	}
	else
		rightPressed = true;
	this->mousePos.setX(mouseX);
	this->mousePos.setY(mouseY);
}

void Inputs::handleMouseUp(int button, float mouseX, float mouseY)
{
	if(button == 0){
		leftPressed = false;
	}
	else
		rightPressed = false;
	this->mousePos.setX(mouseX);
	this->mousePos.setY(mouseY);
}

void Inputs::handleMouseMove(float mouseX, float mouseY)
{
	this->mousePos.setY(mouseY);
	this->mousePos.setX(mouseX);
}

void Inputs::handleKeyDown(int key)
{
	keys[key] = true;
}

void Inputs::handleKeyUp(int key)
{
	keys[key] = false;
}

