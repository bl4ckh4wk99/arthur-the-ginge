#pragma once

#include "Player.h"
#include "DeathObjects.h"

class Enemies : public DeathObjects{
private:
		Vec2 vel;
		int normalVel, chaseVel;
		int dir;
		bool isDead, chasing;
		double frames;
		int startX;
		int endX;
public:

	//constructors
	Enemies(Vec2 ipos, Vec2 isize, int endX);

	Enemies(float x, float y, float width, float height, int endX);

	//getters and setters for vel
	Vec2 getVel();

	void setVelX(float x);

	void setVelY(float y);

	//chase player methods
	bool checkIfChase(Player* p);
	void chase(Player* p, double deltaT, double prevDeltaT);
	//draw methods
	void draw();
	//update methods
	void update(double deltaT, double prevDeltaT);
	//collision check methods
	bool checkForCollisions(GameObject* g, double deltaT, double prevDeltaT);
	bool checkForCollisions(Player* g, double deltaT, double prevDeltaT);
	//death methods
	void setDead();
	bool checkDeath();
	//set textures
	void setTextures();
};