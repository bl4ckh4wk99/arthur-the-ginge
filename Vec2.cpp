#include "Vec2.h"
#pragma once

//default constructor for vec2s
	Vec2::Vec2(){
		x = y = 0.0f;
	}
	//constructor for vec2s using floats
	Vec2::Vec2(float ix, float iy){
		this->x = ix;
		this->y = iy;
	}

	//operator overload on = to set vec2s
		Vec2 Vec2::operator = (const Vec2 &v){
		this -> x = v.x;
		this -> y = v.y;
		return *this;
		}
	
	//operator overload on +=
	Vec2& Vec2::operator += (const Vec2 &v){
		this->x = x+v.x;
		this->y = y+v.y;
		return *this;
	}
	//operator overload on +
	Vec2& Vec2::operator + (const Vec2 &v){
		return Vec2(x + v.x, y + v.y);
	}
	//operator overload on -
	Vec2& Vec2::operator - (const Vec2 &v){
		return Vec2(x - v.x, y - v.y);
	}
	//operator overload on -=
	Vec2& Vec2::operator -= (const Vec2 &v){
		this->x = x - v.x;
		this->y = y - v.y;
		return *this;
	}
	//getter for x
	float Vec2::getX(){
		return this->x;
	}
	//setter for x
	void Vec2::setX(float x){
		this->x = x;
	}
	//getter y
	float Vec2::getY(){
		return this->y;
	}
	//setter for y
	void Vec2::setY(float y){
		this->y = y;
	}
