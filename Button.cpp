#include "Button.h"


Button::Button(Vec2 pos, Vec2 size, char* tex1, char* tex2, int ID){
	this->pos = pos;
	this->size = size;
	idle_textures.push_back(loadPNG(tex1));
	idle_textures.push_back(loadPNG(tex2));
	selected = false;
	this->ID = ID;
}

Button::Button(float x, float y, float width, float height, char* tex1, char* tex2, int ID){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(width, height);
	this->pos = pos;
	this->size = size;
	idle_textures.push_back(loadPNG(tex1));
	idle_textures.push_back(loadPNG(tex2));
	selected = false;
	this->ID = ID;
}

bool Button::isSelected(){
	return this->selected;
}

void Button::setSelected(bool i){
	this->selected = i;
}

int Button::getID(){
	return this->ID;
}

void Button::draw(){
	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
			glTexCoord2f(0.0, 1); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(1, 1); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(1, 0.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	//circle.draw();
	//box.draw();
}

//update method
void Button::update(double deltaT, double prevDeltaT){
	if (!selected)
		currentTexture = idle_textures.front();
	else
		currentTexture = idle_textures.back();
	//get its x and y pos
	pos.getX();
	pos.getY();
	//update its collidables
	box.update(this->pos);
	circle.update(this->pos);
}

//set collidables for this object
void Button::setCollidables(){
	//create bounding box
	BoundingBox box = BoundingBox(this->pos, this->size);

	this->box = box;
	//create bounding circle
	float radius = pow((this->size.getX() / 2), 2) + pow((this->size.getY() / 2), 2);
	radius = sqrt(radius);
	BoundingCircle circle = BoundingCircle(this->pos, radius);
	this->circle = circle;
}

bool Button::checkForCollisions(float mouseX, float mouseY){
	//cout << this->box.getPos().getX() - this->box.getSize().getX() / 2 << endl;
	//cout << this->box.getPos().getX() + this->box.getSize().getX() / 2 << endl;
	//cout << this->box.getPos().getY() - this->box.getSize().getY() / 2 << endl;
	//cout << this->box.getPos().getY() + this->box.getSize().getY() / 2 << endl;

	if (mouseX > this->box.getPos().getX() - this->box.getSize().getX() / 2 &&
		mouseX < this->box.getPos().getX() + this->box.getSize().getX() / 2 &&
		mouseY > this->box.getPos().getY() - this->box.getSize().getY() / 2 &&
		mouseY < this->box.getPos().getY() + this->box.getSize().getY() / 2){

		setSelected(true);
		return true;
	}
	
	setSelected(false);
	return false;

}

void Button::setTextures(){}

