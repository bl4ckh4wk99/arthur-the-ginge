#pragma once

#include <Windows.h>
#include "glew.h"
#include "nvImage.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library



class LoadPNG{

public:
	//constructors
	LoadPNG();
	// method that loads a PNG
	GLuint loadPNG(const char * filename);

};