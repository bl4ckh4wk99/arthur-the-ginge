#pragma once

#include "GameObject.h"
#include "Camera.h"

class Hud : public GameObject{
private:
	vector<GLuint> numbers;

	//Clock
	int tensM, M, tensS, S;
	double time;
	GLuint tensMin, Min, tensSec, Sec, colon;

	//Death Counter
	int counter;
	GLuint deathSymbol, tens, units;

	//Gem Counter
	int gems;
	GLuint gemCounter, gTens, gUnits;

public:
	//constructor
	Hud(int time);
	//draw method
	void draw();
	//update methods
	void update(double deltaT, double prevDeltaT);
	void updatePos(Camera* c);
	//set collidables
	void setCollidables();
	//set textures
	void setTextures();

	//Death Counter methods
	void incrementCounter();
	int getNumDeaths();

	//Gem Counter methods
	void incrementGems();
	int getNumGems();

	//Clock methods
	int getTimeInSec();
	bool checkTime();
	void gemCollected();
};