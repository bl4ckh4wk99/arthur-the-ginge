#pragma once
//
// This is derived from the Graphics 1 OpenGL
//

#include "Activity.h"


// END SCREEN ACTIVITY

class EndActivity : public Activity
{
private:
	// We need a texture ID for our start screen texture
	GLuint textureID;

public:
	EndActivity(OpenGLApplication *app);

	// ACTIVITY METHODS
	virtual void initialise();											// Called on application start up
	virtual void finish();												// Called when the activity is finished and needs to be rebuilt from scratch
	virtual void shutdown();											// Called on application shut down

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onSwitchOut();											// Activity switch in; called when the activity changes and this one switches out
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void draw();
	
	// Event handler methods; these methods are called in response to user actions:
	void onMouseDown(int button, float mouseX, float mouseY);			// Called when mouse button pressed
	void onMouseUp(int button, float mouseX, float mouseY);				// Called when mouse button released
	void onMouseMove(float mouseX, float mouseY);						// Called when mouse moved
	void onKeyDown(int key);										// Called when key pressed
	void onKeyUp(int key);											// Called when key released
};