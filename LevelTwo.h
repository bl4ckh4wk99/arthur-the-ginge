//
// This is derived from the Graphics 1 OpenGL template
//

// Ensure that this file can only be included once
#pragma once

#include "Activity.h"
#include "Player.h"
#include "Camera.h"
#include "Hud.h"
#include "Exit.h"
#include "Saw.h"
#include "Enemies.h"
#include "Follower.h"
#include "LoadPNG.h"
#include "Torch.h"
#include "Gem.h"


// GAME ACTIVITY

class LevelTwo : public Activity
{
private:
	//list of all the objects needed for the level
	Player* player;
	Camera* camera;
	Hud* hud;
	Exit* exit;
	vector<Platforms *> platformObjects;
	vector<MovingPlatforms *> mPlatformObjects;
	vector<Traps *> traps;
	vector<Saw*> saws;
	vector<Enemies *> enemies;
	vector<Follower *> followers;
	vector<Torch*> torches;
	vector<Gem*> gems;
	bool paused;
	LoadPNG imageLoader;
	//background textures
	GLuint background1, background2, background3, background4, door, grassPlat, dirt, brickPlat, brickBase;
public:

	LevelTwo(OpenGLApplication *app);

	virtual void initialise();											// Called on application start up
	virtual void shutdown();											// Called on application shut down
	virtual void finish();												// Called when the activity is finished and needs to be rebuilt from scratch

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onSwitchOut();
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void update(double deltaT, double prevDeltaT);				// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2
	// deltaT is the time elapsed from frame F-1 to frame F, prevDeltaT is the time elapsed from F-2 to F-1
	virtual void draw();												// Render function
	virtual void drawBackground();										//Render background textures


	// Event handler methods; these methods are called in response to user actions:
	void onKeyDown(int key);									// Called when key pressed
	void onKeyUp(int key);										// Called when key released
	void onMouseDown(int button, float mouseX, float mouseY);		// Called when mouse button pressed
	void onMouseUp(int button, float mouseX, float mouseY);			// Called when mouse button released
	void onMouseMove(float mouseX, float mouseY);					// Called when mouse moved


};