//
// This is derived from the Graphics 1 OpenGL template
//


//Level 2 implementation

#include <windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library


#include "OpenGLApplication.h"			// Needed to access member functions and variables from OpenGLApplication
#include "LevelTwo.h"

#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE

// hStart, hMid, hEnd, vStart, vMid, vEnd;

LevelTwo::LevelTwo(OpenGLApplication *app)
	: Activity(app)		// Call super constructor
{paused = false;}


void LevelTwo::initialise()
{

	onSwitchIn();

	hud = new Hud(120);
	hud->setTextures();

	LoadPNG imageLoader = LoadPNG();
	background1 = imageLoader.loadPNG("World_textures/background.png");
	background2 = imageLoader.loadPNG("World_textures/background2.png");
	background3 = imageLoader.loadPNG("World_textures/BrickBack.png");
	background4 = imageLoader.loadPNG("World_textures/layer-1.png");
	grassPlat = imageLoader.loadPNG("World_textures/Tile.png");
	dirt = imageLoader.loadPNG("World_textures/Brick.png");
	brickPlat = imageLoader.loadPNG("World_textures/BrickTile.png");
	brickBase = imageLoader.loadPNG("World_textures/Brick.png");
	door = imageLoader.loadPNG("World_textures/door5.png");

	Gem* gem1 = new Gem(70, 1);
	gems.push_back(gem1);

	Gem* gem2 = new Gem(110, 12);
	gems.push_back(gem2);

	Gem* gem3 = new Gem(145, 12);
	gems.push_back(gem3);

	Gem* gem4 = new Gem(145, -48);
	gems.push_back(gem4);

	Gem* gem5 = new Gem(87, -25);
	gems.push_back(gem5);

	Gem* gem6 = new Gem(65, -36);
	gems.push_back(gem6);

	Gem* gem7 = new Gem(55, -76);
	gems.push_back(gem7);

	Gem* gem8 = new Gem(75, -76);
	gems.push_back(gem8);

	Gem* gem9 = new Gem(88, -70);
	gems.push_back(gem9);

	Gem* gem10 = new Gem(150, -68);
	gems.push_back(gem10);
}


void LevelTwo::shutdown()
{
	traps.clear();
	platformObjects.clear();
	enemies.clear();
	mPlatformObjects.clear();
	saws.clear();
	followers.clear();
	torches.clear();

	camera = NULL;
	player = NULL;
	exit = NULL;
	hud = NULL;
}



void LevelTwo::onSwitchIn()
{

	glClearColor(0.0, 0.0, 0.0, 0.0);						//sets the clear colour to black

	//********************************************************
	//Floor 3
	//********************************************************
	Platforms* base1 = new Platforms(90, -87, 180, 2, brickBase);
	platformObjects.push_back(base1);

	Platforms* floor1 = new Platforms(10, -80, 20, 2, brickPlat);
	platformObjects.push_back(floor1);

	Torch* torch1 = new Torch(20,-72);
	torches.push_back(torch1);

	Enemies* zom1 = new Enemies(19, -79, 2, 2, 10);
	zom1->setTextures();
	enemies.push_back(zom1);

	Traps* lava1 = new Traps(100, -83, 200, 6);
	lava1->setTextures("Trap_textures/lava.png");
	traps.push_back(lava1);

	Platforms* plat1 = new Platforms(35, -75, 20, 2, brickPlat);
	platformObjects.push_back(plat1);

	Torch* torch2 = new Torch(48,-72);
	torches.push_back(torch2);

	Enemies* zom2 = new Enemies(26, -74, 2, 2, 34);
	zom2->setTextures();
	enemies.push_back(zom2);

	Enemies* zom3 = new Enemies(44, -74, 2, 2, 36);
	zom3->setTextures();
	enemies.push_back(zom3);

	Platforms* plat2 = new Platforms(57, -69, 15, 2, brickPlat);
	platformObjects.push_back(plat2);

	Platforms* plat3 = new Platforms(55, -78, 10, 2, brickPlat);
	platformObjects.push_back(plat3);

	Platforms* plat4 = new Platforms(75, -78, 10, 2, brickPlat);
	platformObjects.push_back(plat4);

	Torch* torch3 = new Torch(66,-72);
	torches.push_back(torch3);

	Saw* saw1 = new Saw(74, -75, 2.5, 0, -60);
	saw1->setTextures();
	saws.push_back(saw1);

	Platforms* plat5 = new Platforms(90, -72, 10, 2, brickPlat);
	platformObjects.push_back(plat5);

	Torch* torch4 = new Torch(98,-72);
	torches.push_back(torch4);

	Platforms* plat6 = new Platforms(105, -68, 10, 2, brickPlat);
	platformObjects.push_back(plat6);

	Enemies* zom4 = new Enemies(86, -71, 2, 2, 94);
	zom4->setTextures();
	enemies.push_back(zom4);

	Enemies* zom5 = new Enemies(101, -67, 2, 2, 109);
	zom5->setTextures();
	enemies.push_back(zom5);

	Platforms* plat7 = new Platforms(105, -78, 8, 2, brickPlat);
	platformObjects.push_back(plat7);

	Torch* torch5 = new Torch(125,-72);
	torches.push_back(torch5);

	Saw* saw2 = new Saw(115, -74, 2.5, 0, -60);
	saw2->setTextures();
	saws.push_back(saw2);

	Platforms* floor2 = new Platforms(125, -80, 15, 2, brickPlat);
	platformObjects.push_back(floor2);

	Enemies* zom6 = new Enemies(118, -67, 2, 2, 132);
	zom6->setTextures();
	enemies.push_back(zom6);

	Platforms* plat8 = new Platforms(145, -75, 8, 2, brickPlat);
	platformObjects.push_back(plat8);

	Torch* torch6 = new Torch(153,-72);
	torches.push_back(torch6);

	Platforms* plat9 = new Platforms(160, -70, 8, 2, brickPlat);
	platformObjects.push_back(plat9);


	MovingPlatforms* mPlat1 = new MovingPlatforms(170, -70, 4, 2, 0, -40);
	mPlat1->setTextures();
	mPlatformObjects.push_back(mPlat1);

	Torch* torch7 = new Torch(175,-55);
	torches.push_back(torch7);

	

	//********************************************************
	//Floor 2
	//********************************************************

	Platforms* floor3 = new Platforms(160, -50, 15, 2, brickPlat);
	platformObjects.push_back(floor3);

	MovingPlatforms* mPlat2 = new MovingPlatforms(145, -50, 4, 2, 0, -20);
	mPlat2->setTextures();
	mPlatformObjects.push_back(mPlat2);

	Torch* torch8 = new Torch(155,-35);
	torches.push_back(torch8);

	Platforms* plat10 = new Platforms(160, -25, 10, 2, brickPlat);
	platformObjects.push_back(plat10);

	if (followers.size() < 1){
		Follower* follow1 = new Follower(160, -23, 2, 2, 1);
		follow1->setTextures();
		followers.push_back(follow1);
	}

	Platforms* plat11 = new Platforms(130, -40, 15, 2, brickPlat);
	platformObjects.push_back(plat11);

	Saw* saw3 = new Saw(120, -50, 2.5, 0, -25);
	saw3->setTextures();
	saws.push_back(saw3);

	Torch* torch9 = new Torch(115,-35);
	torches.push_back(torch9);

	Platforms* plat12 = new Platforms(105, -32, 10, 2, brickPlat);
	platformObjects.push_back(plat12);

	MovingPlatforms* mPlat3 = new MovingPlatforms(60, -38, 4, 2, 115, 0);
	mPlat3->setTextures();
	mPlatformObjects.push_back(mPlat3);

	Platforms* plat13 = new Platforms(88, -27, 10, 2, brickPlat);
	platformObjects.push_back(plat13);

	Torch* torch10 = new Torch(85,-35);
	torches.push_back(torch10);

	Saw* saw4 = new Saw(80, -36, 2.5, 0, -15);
	saw4->setTextures();
	saws.push_back(saw4);

	Platforms* plat14 = new Platforms(65, -30, 15, 2, brickPlat);
	platformObjects.push_back(plat14);

	Enemies* zom7 = new Enemies(59, -29, 2, 2, 71);
	zom7->setTextures();
	enemies.push_back(zom7);

	Torch* torch11 = new Torch(60,-35);
	torches.push_back(torch11);

	Platforms* plat15 = new Platforms(45, -45, 10, 2, brickPlat);
	platformObjects.push_back(plat15);

	MovingPlatforms* mPlat4 = new MovingPlatforms(35, -43, 4, 2, 0, -25);
	mPlat4->setTextures();
	mPlatformObjects.push_back(mPlat4);

	Torch* torch12 = new Torch(40,-35);
	torches.push_back(torch12);

	Platforms* plat16 = new Platforms(25, -25, 10, 2, brickPlat);
	platformObjects.push_back(plat16);

	MovingPlatforms* mPlat5 = new MovingPlatforms(10, -25, 4, 2, 0, 0);
	mPlat5->setTextures();
	mPlatformObjects.push_back(mPlat5);

	Torch* torch13 = new Torch(15,-20);
	torches.push_back(torch13);
	
	Platforms* leftWall = new Platforms(-1, 0, 2, 300, brickBase);
	platformObjects.push_back(leftWall);

	//********************************************************
	//Floor 1
	//********************************************************
	Platforms* floor4 = new Platforms(30, -1, 30, 2, grassPlat);
	platformObjects.push_back(floor4);

	Enemies* zom8 = new Enemies(17, 1, 2, 2, 43);
	zom8->setTextures();
	enemies.push_back(zom8);

	Platforms* base3 = new Platforms(100, -9, 170, 2, brickBase);
	platformObjects.push_back(base3);

	Traps* spikes = new Traps(50, -7, 10, 2);
	spikes->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes);

	Platforms* plat17 = new Platforms(35, 5, 10, 2, grassPlat);
	platformObjects.push_back(plat17);

	Enemies* zom9 = new Enemies(38, 6, 2, 2, 32);
	zom9->setTextures();
	enemies.push_back(zom9);

	Platforms* plat18 = new Platforms(22, 10, 10, 2, grassPlat);
	platformObjects.push_back(plat18);

	Enemies* zom10 = new Enemies(19, 11, 2, 2, 25);
	zom10->setTextures();
	enemies.push_back(zom10);

	Platforms* plat19 = new Platforms(5, 16, 10, 2, grassPlat);
	platformObjects.push_back(plat19);

	if (followers.size() < 2){
		Follower* follow2 = new Follower(5, 18, 2, 2, 2);
		follow2->setTextures();
		followers.push_back(follow2);
	}

	Platforms* floor5 = new Platforms(70, -1, 30, 2, grassPlat);
	platformObjects.push_back(floor5);

	Enemies* zom11 = new Enemies(69, 1, 2, 2, 57);
	zom11->setTextures();
	enemies.push_back(zom11);

	Enemies* zom12 = new Enemies(71, 1, 2, 2, 83);
	zom12->setTextures();
	enemies.push_back(zom12);

	Platforms* plat23 = new Platforms(70, 6, 26, 2, grassPlat);
	platformObjects.push_back(plat23);

	Platforms* plat20 = new Platforms(95, 5, 8, 2, grassPlat);
	platformObjects.push_back(plat20); 

	Traps* spikes2 = new Traps(95, -7, 20, 2);
	spikes2->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes2);

	Platforms* plat21 = new Platforms(110, 10, 8, 2, grassPlat);
	platformObjects.push_back(plat21); 

	Platforms* floor6 = new Platforms(115, -1, 20, 2, grassPlat);
	platformObjects.push_back(floor6);

	Enemies* zom13 = new Enemies(107, 1, 2, 2, 114);
	zom13->setTextures();
	enemies.push_back(zom13);

	Enemies* zom14 = new Enemies(123, 1, 2, 2, 116);
	zom14->setTextures();
	enemies.push_back(zom14);

	Traps* spikes3 = new Traps(143, -7, 36, 2);
	spikes3->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes3);

	Platforms* plat22 = new Platforms(136, 5, 8, 2, grassPlat);
	platformObjects.push_back(plat22);
	
	Platforms* floor7 = new Platforms(152, -1, 2, 2, grassPlat);
	platformObjects.push_back(floor7);

	Platforms* floor8 = new Platforms(170, -1, 20, 2, grassPlat);
	platformObjects.push_back(floor8);

	Platforms* rightWall = new Platforms(181, 0, 2, 300, brickBase);
	platformObjects.push_back(rightWall);

	//**************************************************************
	//************************FILLER********************************
	//**************************************************************
	Platforms* filler1 = new Platforms(30, -6, 30, 8, brickBase);
	platformObjects.push_back(filler1);

	Platforms* filler2 = new Platforms(70, -6, 30, 8, brickBase);
	platformObjects.push_back(filler2);

	Platforms* filler3 = new Platforms(115, -6, 20, 8, brickBase);
	platformObjects.push_back(filler3);

	Platforms* filler4 = new Platforms(170, -6, 20, 8, brickBase);
	platformObjects.push_back(filler4);
	

	exit = new Exit(170, 5, 8, 10);
	exit->setTextures();

	player = new Player(8, -75, 2, 2);
	camera = new Camera(8, -75);
	if (followers.back()->isActive()){
		player->setPos(5, 18);
		camera->setPos(5, 18);
	}
	else if (followers.front()->isActive()){
		player->setPos(160, -23);
		camera->setPos(160, -23);
	}
	player->setTextures();

}

void LevelTwo::onSwitchOut(){
	traps.clear();
	platformObjects.clear();
	enemies.clear();
	mPlatformObjects.clear();
	saws.clear();
	torches.clear();
}

void LevelTwo::finish(){
	traps.clear();
	platformObjects.clear();
	enemies.clear();
	mPlatformObjects.clear();
	saws.clear();
	followers.clear();
	torches.clear();

	camera = NULL;
	player = NULL;
	exit = NULL;
	hud = NULL;
}

void LevelTwo::onReshape(int w, int h)
{

	glViewport(0, 0, w, h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();;

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	//gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect,  -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glOrtho(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect, -VIEW_SIZE*0.5, VIEW_SIZE*0.5, -VIEW_SIZE - 1, VIEW_SIZE);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

void LevelTwo::update(double deltaT, double prevDeltaT)
{

	if (!paused){
		glClear(GL_COLOR_BUFFER_BIT);

		player->setCollidables();
		player->update(deltaT, prevDeltaT);
		for each(Follower* f in followers){
			f->setCollidables();
			f->update(deltaT, prevDeltaT, player);
			f->checkForCollisions(player, deltaT, prevDeltaT);
		}
		for each(Platforms* s in platformObjects){
			s->setCollidables();
			s->update(deltaT, prevDeltaT);
			player->checkForCollisions(s, deltaT, prevDeltaT);
		}
		for each(MovingPlatforms* mp in mPlatformObjects){
			mp->setCollidables();
			mp->update(deltaT,prevDeltaT);
			player->checkForCollisions(mp, deltaT, prevDeltaT);
		}
		for each(Traps* d in traps){
			d->setCollidables();
			d->update(deltaT, prevDeltaT);
			player->checkForCollisions(d, deltaT, prevDeltaT);
		}
		for each(Saw* s in saws){
			s->setCollidables();
			s->update(deltaT, prevDeltaT);
			s->checkForCollisions(player,deltaT,prevDeltaT);
		}
		for each(Enemies* e in enemies){
			if(!e->checkDeath()){
				e->setCollidables();
				bool chase = e->checkIfChase(player);
				if (chase){
					e->chase(player, deltaT, prevDeltaT);
				}
			e->update(deltaT, prevDeltaT);
			e->checkForCollisions(player, deltaT, prevDeltaT);
			}
			for each(Traps* d in traps){
				e->checkForCollisions(d, deltaT, prevDeltaT);
			}
			for each(Platforms* s in platformObjects){
				e->checkForCollisions(s, deltaT, prevDeltaT);
			}

		}
		for each(Torch* t in torches){
			t->update(deltaT, prevDeltaT);
		}
		for each(Gem* g in gems){
			if (!g->checkCollected()){
				g->update(deltaT, prevDeltaT);
				if (g->checkForCollisions(player, deltaT, prevDeltaT)){
					hud->gemCollected();
				}
			}
		}
		camera->updatePos(deltaT, prevDeltaT, player);
		exit->setCollidables();
		exit->update(deltaT, prevDeltaT);
		hud->setCollidables();
		hud->update(deltaT, prevDeltaT);
		hud->updatePos(camera);
	}
	
	if (exit->checkForCollisions(player, deltaT, prevDeltaT))
		app->finishLevel(hud, 2);
	else{
		if (!hud->checkTime()){
			app->timeRanOut();
		}
	}


	glFlush;
}



void LevelTwo::draw()
{

	if (!paused){
		glClear(GL_COLOR_BUFFER_BIT);

		glLoadIdentity();
		gluLookAt(camera->getPos().getX(), camera->getPos().getY(), 1.0f, camera->getPos().getX(), camera->getPos().getY(), 0.0f, 0, 1, 0);
		//cout << "cam X : " << camera->getPos().getX() << endl;
		//cout << "cam Y : " << camera->getPos().getY() << endl;
		drawBackground();

		exit->draw();

		
		for each(Traps* t in traps){
			t->draw();
		}
		for each(Enemies* e in enemies){
			e->draw();
		}
		for each(MovingPlatforms* mp in mPlatformObjects){
			mp->draw();
		}
		for each(Saw* s in saws){
			s->draw();
		}
		for each(Platforms* s in platformObjects){
			s->draw();
		}
		for each(Torch* t in torches){
			t->draw();
		}
		for each(Gem* g in gems){
			g->draw();
		}
		for each(Follower* f in followers){
			f->draw();
		}
		hud->draw();
		
		//player is alive
		if (!player->checkLife())
			player->draw();
		//player is dead
		else{
			hud->incrementCounter();
			app->playerDied();
		}
	}

	glFlush;
}

void LevelTwo::drawBackground(){
	double aspect = app->getAspectRatio();

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background4);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-25, 49);
			glTexCoord2f(0, 1); glVertex2f(-25, 24);
			glTexCoord2f(250/25, 1); glVertex2f(225, 24);
			glTexCoord2f(250/25, 0); glVertex2f(225, 49);
		glEnd();
		//pop the matrix
	glPopMatrix();
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background1);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-25, 0);
			glTexCoord2f(0, 1); glVertex2f(-25, 25);
			glTexCoord2f(250/25, 1); glVertex2f(225, 25);
			glTexCoord2f(250/25, 0); glVertex2f(225, 0);
		glEnd();
		//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background2);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-25, -10);
			glTexCoord2f(0, 1); glVertex2f(-25, 0);
			glTexCoord2f(225 / 10, 1); glVertex2f(225, 0);
			glTexCoord2f(225 / 10, 0); glVertex2f(225, -10);
		glEnd();
		//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	
	
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background3);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
				glTexCoord2f(0.0, 0.0);glVertex2f(-25,-10);
				glTexCoord2f(0.0, 50);glVertex2f(-25,-100);
				glTexCoord2f(112.5,50);glVertex2f(225,-100);
				glTexCoord2f(112.5, 0.0);glVertex2f(225,-10);
		glEnd();
	//pop the matrix

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, door);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(4, -79);
			glTexCoord2f(0.0, 1.0); glVertex2f(4, -69);
			glTexCoord2f(1.0, 1.0); glVertex2f(12, -69);
			glTexCoord2f(1.0, 0.0); glVertex2f(12, -79);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}



void LevelTwo::onMouseDown(int button, float mouseX, float mouseY){}		// Called when mouse button pressed
void LevelTwo::onMouseUp(int button, float mouseX, float mouseY){}		// Called when mouse button released
void LevelTwo::onMouseMove(float mouseX, float mouseY){}					// Called when mouse moved

void LevelTwo::onKeyDown(int key){									// Called when key pressed

	if (key == 0x25 || key == 65){
		player->setVelX(-100.0f);
		player->setDirection(-1);

	}
	if (key == 0x27 || key == 68){
		player->setVelX(100.0f);
		player->setDirection(1);

	}
	if (key == 0x26 || key == ' '){
		if (!player->getJump()){
			player->setVelY(250.0f);
			player->setJump(true);
		}
	}

}
void LevelTwo::onKeyUp(int key){									// Called when key released

	if (key == 80){
		paused = !paused;
	}


	//if the velocity of x is greater than 0 slowly decrease it
	if (key == 0x25 || key == 65){//&& movingObjects.front()->getVel().getX() < 0){
		player->setVelX(0.0f);
	}
	//else slowly increase it
	if (key == 0x27 || key == 68){// && movingObjects.front()->getVel().getX() > 0){
		player->setVelX(0.0f);
	}


}
