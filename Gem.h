#pragma once

#include <windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library
#include <string>
#include "GameObject.h"
#include "Player.h"
#include "BoundingCircle.h"

#include <iostream>
using namespace std;

class Gem : public GameObject{
private:
	bool collected;
public:
	//constrcutors
	Gem(Vec2 pos);
	Gem(float x, float y);
	//check to see if collected
	bool checkCollected();
	//draw method
	void draw();
	//update method
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//check collisions
	bool checkForCollisions(Player* g, double deltaT, double prevDeltaT);
	//set textures
	void setTextures();

};