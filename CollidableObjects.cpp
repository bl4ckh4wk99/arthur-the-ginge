#pragma once
#include "CollidableObjects.h"

//default constructor for collidable objects
CollidableObjects::CollidableObjects(){}
//constructor for collidable objects using vec2s
CollidableObjects::CollidableObjects(Vec2 ipos, Vec2 isize){
	this->pos = ipos;
	this->size = isize;
}
//constructor for collidable objects using floats
CollidableObjects::CollidableObjects(float x, float y, float width, float height){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(width, height);
	this->pos = pos;
	this->size = size;
}

//getter for positions
Vec2 CollidableObjects::getPos(){
	return this->pos;
}
//setter for position
void CollidableObjects::setPos(float x, float y){
	Vec2 newPos = Vec2(x, y);
	this->pos = newPos;
}
//getter for size
Vec2 CollidableObjects::getSize(){
	return this->size;
}
//setter for size
void CollidableObjects::setSize(float width, float height){
	Vec2 newSize = Vec2(width, height);
	this->size = newSize;
}