#include "StaticWorld.h"
#include <sstream>


	//draw method
	void StaticWorld::draw(){
		//push the matrix
		glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
				glTexCoord2f(0.0, 0.0);glVertex2f(this->pos.getX() - this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
				glTexCoord2f(0.0, this->size.getY()/2); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
				glTexCoord2f(this->size.getX() /2, this->size.getY()/2); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
				glTexCoord2f(this->size.getX()/2, 0.0);glVertex2f(this->pos.getX() + this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
		glEnd();
		//pop the matrix
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		//circle.draw();
		//box.draw();
	}
	//update method
	void StaticWorld::update(double deltaT, double prevDeltaT){
		//get its x and y pos
		pos.getX();
		pos.getY();
		//update its collidables
		box.update(this->pos);
		circle.update(this->pos);
	}
	
	//set collidables for this object
	void StaticWorld::setCollidables(){
		//create bounding box
		BoundingBox box = BoundingBox(this->pos, this->size);

		this->box = box;
		//create bounding circle
		float radius = pow((this->size.getX() / 2), 2) + pow((this->size.getY() / 2), 2);
		radius = sqrt(radius);
		BoundingCircle circle = BoundingCircle(this->pos, radius);
		this->circle = circle;
	}

	void StaticWorld::setTextureFile(char* file){
		textureFile = file;
		setTextures();
	}


	void StaticWorld::setTextures(){
		GLuint idle1 = loadPNG(textureFile);                                      
		this->addIdleTexture(idle1);

	}
