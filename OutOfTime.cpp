//
// This is derived from the Graphics 1 OpenGL template
//

#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls
#include "OutOfTime.h"
#include "LoadPNG.h"

#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE
//constructor
OutOfTime::OutOfTime(OpenGLApplication *app)
	: Activity(app)
{
}
//initialise method, called at the start of the program
void OutOfTime::initialise()
{
	onSwitchIn();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

//shutdown method, called at the end of the program
void OutOfTime::shutdown()
{
	Buttons.clear();
	glDeleteTextures(1, &textureID);
}

//switch out method, called when switching away from this activity
void OutOfTime::onSwitchOut()
{
	shutdown();
}

//switch in method, called when switching to this activity
void OutOfTime::onSwitchIn()
{
	// Activity switched in

	glClearColor(0.0, 0.0, 0.0, 0.0);						//sets the clear colour to black

	// Initialise the activity; called at application start up
	LoadPNG imageloader = LoadPNG();
	// Load the start screen image as a texture using the SOIL library
	textureID = imageloader.loadPNG("Background_textures/TimeScreen.png");
	background = imageloader.loadPNG("World_textures/Full-background.png");

	Button* menu = new Button(11, -10, 20, 5, "Background_textures/Menu.png", "Background_textures/Menu-select.png", 1);
	Buttons.push_back(menu);

	Button* startAgain = new Button(-11, -10, 20, 5, "Background_textures/StartAgain.png", "Background_textures/StartAgain-select.png", 2);
	Buttons.push_back(startAgain);
}
//finish method, called when this activity is finished
void OutOfTime::finish()
{
	shutdown();
}
//reshape method, reshapes the windows for this method
void OutOfTime::onReshape(int w, int h)
{
	glViewport(0, 0, w, h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();;

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect, -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();								// Reset The Modelview Matrix
}
//update method for this activity
void OutOfTime::update(double deltaT, double prevDeltaT){
	for each(Button* b in Buttons){
		b->setCollidables();
		b->update(deltaT, prevDeltaT);
	}
}
//draw method for this activity
void OutOfTime::draw()
{
	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	double aspect = app->getAspectRatio();
	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, background);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
		glEnd();

	//pop the matrix
	glPopMatrix();


	glPushMatrix();
		// Identity matrix
		glLoadIdentity();


		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, textureID);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5 + 5);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5 + 5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	// Disable 2D texturing
	glDisable(GL_TEXTURE_2D);

	for each(Button* b in Buttons){
		b->draw();
	}

	glFlush();
}

//mouse button method for this activity
void OutOfTime::onMouseDown(int button, float mouseX, float mouseY){		// Called when mouse button pressed

}
//mouse button method for this activity
void OutOfTime::onMouseUp(int button, float mouseX, float mouseY){			// Called when mouse button released
	for (int i = 0; i < Buttons.size(); i++){
		Button* b = Buttons.at(i);
		if (b->isSelected() && b->getID() == 1 && button == 0){
			app->exitLevel();
		}
		if (b->isSelected() && b->getID() == 2 && button == 0){
			app->restartLevel();
		}
	}
}
//mouse movement method for this activity
void OutOfTime::onMouseMove(float mouseX, float mouseY){					// Called when mouse moved
	double aspect = app->getAspectRatio();
	double oriAspect = app->getOriAspectRatio();
	float x = ((mouseX - app->getScreenWidth() / 2) / (VIEW_SIZE*0.5*oriAspect*(oriAspect / aspect) * ((double)app->getScreenWidth() / (double)app->getOriScreenWidth())));
	float y = ((app->getScreenHeight() / 2 - mouseY) / (VIEW_SIZE*0.5*oriAspect* ((double)app->getScreenHeight() / (double)app->getOriScreenHeight())));
	for each(Button* b in Buttons){
		b->checkForCollisions(x, y);
	}
}
//key button method for this activity
void OutOfTime::onKeyDown(int key){										// Called when key released

}
//key button method for this activity
void OutOfTime::onKeyUp(int key){										// Called when key released
	if (key == ' ')
	{
		app->restartLevel();
	}

}

