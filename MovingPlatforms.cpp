#include "MovingPlatforms.h"

MovingPlatforms::MovingPlatforms(Vec2 ipos, Vec2 isize, float iendX, float iendY){
	this->pos = ipos;
	this->size = isize;
	if(iendX != 0){
		this->vel.setX(50.0);
		this->startX = ipos.getX();
		this->endX = iendX;
		horizontal = true;
	}
	else{
		this->vel.setY(50.0);
		this->startY = ipos.getY();
		this->endY = iendY;
		horizontal = false;
	}
}

MovingPlatforms::MovingPlatforms(float x, float y, float width, float height, float iendX, float iendY){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(width,height);
	this->pos = pos;
	this->size = size;
	if(iendX != 0){
		this->vel.setX(50.0);
		this->startX = x;
		this->endX = iendX;
		horizontal = true;
	}
	else{
		this->vel.setY(50.0);
		this->startY = y;
		this->endY = iendY;
		horizontal = false;
	}
}

void MovingPlatforms::setTextures(){
	currentTexture = loadPNG("World_textures/mplatform.png");
	idle_textures.push_back(currentTexture);
	hStart = loadPNG("Trap_textures/hSlider1.png");
	hMid = loadPNG("Trap_textures/hSlider2.png");
	hEnd = loadPNG("Trap_textures/hSlider3.png");
	vStart = loadPNG("Trap_textures/vSlider3.png");
	vMid = loadPNG("Trap_textures/vSlider2.png");
	vEnd = loadPNG("Trap_textures/vSlider1.png");
}

void MovingPlatforms::draw(){
	if(horizontal)
		drawHSlider();
	else
		drawVSlider();
	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	//circle.draw();
	//box.draw();

}

void MovingPlatforms::drawHSlider(){
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, hStart);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(startX,this->pos.getY()-0.5);
			glTexCoord2f(0.0, 1.0); glVertex2f(startX, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 1.0); glVertex2f(startX + 5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 0.0); glVertex2f(startX + 5, this->pos.getY()-0.5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, hMid);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(startX + 5, this->pos.getY()-0.5);
			glTexCoord2f(0.0, 1.0); glVertex2f(startX + 5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 1.0); glVertex2f(endX - 5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 0.0); glVertex2f(endX - 5, this->pos.getY()-0.5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, hEnd);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(endX-5, this->pos.getY()-0.5);
			glTexCoord2f(0.0, 1.0); glVertex2f(endX-5, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 1.0); glVertex2f(endX, this->pos.getY()+0.5);
			glTexCoord2f(1.0, 0.0); glVertex2f(endX, this->pos.getY()-0.5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

}

void MovingPlatforms::drawVSlider(){
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, vStart);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()-0.5, startY);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()-0.5, startY+5);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+0.5, startY+5);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+0.5, startY);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, vMid);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()-0.5, startY+5);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()-0.5, endY-5);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+0.5, endY-5);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+0.5, startY+5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, vEnd);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()-0.5, endY-5);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()-0.5, endY);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+0.5, endY);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+0.5, endY-5);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

}

void MovingPlatforms::update(double deltaT, double prevDeltaT){
	currentTexture = idle_textures.front();
	if(horizontal){
		if (pos.getX() < startX || pos.getX() > endX){
			vel.setX(-vel.getX());
		}
	}
	else{
		if (pos.getY() < startY || pos.getY() > endY){
			vel.setY(-vel.getY());
		}
	}

	//set new x and y poisitions dependant on the velocity
	pos.setX(pos.getX() + vel.getX() / 10 * deltaT);

	pos.setY(pos.getY() + vel.getY() / 10 * deltaT);

	//update its collidables
	box.update(this->pos);
	circle.update(this->pos);

}

Vec2 MovingPlatforms::getVel(){
	return this->vel;
}

void MovingPlatforms::setVelX(float x){
	this->vel.setX(x);
}
void MovingPlatforms::setVelY(float y){
	this->vel.setY(y);
}

