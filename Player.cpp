#include "Player.h"

const float g = 0.2f;

//constructor for a player object using vec2s
	Player::Player(Vec2 ipos,Vec2 ivel, Vec2 isize){
		this->pos = ipos;
		this->ori = isize;
		this->vel = ivel;
		this->size = isize;
		this->direction = 1;
		this->isDead = false;
		this->frames = 0.0f;
	}
//constructor for player object using floats
	Player::Player(float x, float y, float width, float height){
		Vec2 pos = Vec2(x,y);
		Vec2 vel = Vec2();
		Vec2 size = Vec2(width,height);
		this->pos = pos;
		this->ori = size;
		this->size = size;
		this->vel = vel;
		this->direction = 1;
		this->isDead = false;
		this->frames = 0.0f;
	}

	//getter for velocity
	Vec2 Player::getVel(){
		return this->vel;
	}
	//setter for velocity X value
	void Player::setVelX(float x){
		this->vel.setX(x);
	}
	//setter for velocity Y value
	void Player::setVelY(float y){
		this->vel.setY(y);
	}

	//getter for direction
	int Player::getDirection(){
		return this->direction;
	}

	//setter for direction
	void Player::setDirection(int newDir){
		this->direction = newDir;
	}
	
	//draw method
	void Player::draw(){
		//push the matrix,
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the vertices using the position and size of the object
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0);glVertex2f(this->pos.getX() - this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
			glTexCoord2f(0.0, 1.0);glVertex2f(this->pos.getX() - this->size.getX()/2, this->pos.getY() + this->size.getY()/2);
			glTexCoord2f(direction, 1.0);glVertex2f(this->pos.getX() + this->size.getX()/2, this->pos.getY() + this->size.getY()/2);
			glTexCoord2f(direction, 0.0);glVertex2f(this->pos.getX() + this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
		glEnd();
		//pop the matrix
		glPopMatrix();

		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		//circle.draw();
		//box.draw();
	}
	//update method
	void Player::update(double deltaT, double prevDeltaT)
	{
		frames += (2*deltaT);

		//get the correct texture

		//running
		if(!jump){
			frames += (5*deltaT);
			int it = (int)frames;
			if(it >= moving_textures.size()){
				it = 0;
				frames = 0;
			}
			currentTexture = moving_textures.at(it);
			this->size.setX(this->ori.getX() * 1.05);
			this->size.setY(this->ori.getY());
		}
		//falling
		if(vel.getY() > 0 && jump){
			int it = (int)frames;
			if(it >= jumping_textures.size()){
				it = 0;
				frames = 0;
			}
			currentTexture = jumping_textures.back();
			this->size = this->ori;
		}
		else
			//jumping
			if(vel.getY() < 0 && jump){
				int it = (int)frames;
				if(it >= jumping_textures.size()){
					it = 0;
					frames = 0;
				}
				currentTexture = jumping_textures.front();
				this->size = this->ori;
			}
			else{
				//idle
				if(vel.getX() == 0){
					int it = (int)frames;
					if(it >= idle_textures.size()){
						it = 0;
						frames = 0;
					}
					currentTexture = idle_textures.at(it);
					this->size = this->ori;
				}
				
			}

		//set new x and y poisitions dependant on the velocity
		pos.setX(pos.getX() + vel.getX()/10 * deltaT);
		
		pos.setY(pos.getY() + vel.getY()/10 * deltaT);

		cout << pos.getX() << "," << pos.getY() << endl;

		this->getVel().setX(0.0f);

		//add gravity velocity in y
		this->vel.setY(this->getVel().getY()-(deltaT * 500));
		//cout << this->getVel().getY() << endl;

		//methods to update and draw each collidable object
		circle.update(this->pos);
		
		box.update(this->pos);
	}

	//set the collidable objects attached to this object
	void Player::setCollidables(){
		//create a bounding box
		BoundingBox box = BoundingBox(this->pos, this->size);
		this->box = box;
		//create a bounding circle
		float radius = pow((this->size.getX() / 2), 2) + pow((this->size.getY() / 2),2);
		radius = sqrt(radius);
		BoundingCircle circle = BoundingCircle(this->pos, radius);
		this->circle = circle;
	}

	//method to check for collisions with a given gameobject
	bool Player::checkForCollisions(Platforms* g, double deltaT, double prevDeltaT){
		//bool variable to store results
		bool collision = false;
		//bool variable for if the circles are colliding
		bool circleTouch = false;
		//get the 2 bounding circles positions and radius
		float x1 = this->circle.getPos().getX();
		float x2 =  g->getCircle().getPos().getX();
		float y1 = this->circle.getPos().getY();
		float y2 = g->getCircle().getPos().getY();
		float r1 = this->circle.getSize().getX();
		float r2 = g->getCircle().getSize().getX();

		//Check circles first
		if((x1 - x2)*(x1-x2) + (y1-y2)*(y1-y2) < (r1+r2)*(r1+r2)){
			circleTouch = true;
		}

		//if circles collided do more thorough checking
		if(circleTouch){
			//get the 2 bounding boxes verticies 
			float xMinA = (this->box.getPos().getX()) - this->box.getSize().getX()/2;
			float xMaxB = g->getBox().getPos().getX() + g->getBox().getSize().getX()/2;
			float xMaxA = (this->box.getPos().getX()) + this->box.getSize().getX()/2;
			float xMinB = g->getBox().getPos().getX() - g->getBox().getSize().getX()/2;
			float yMinA = (this->box.getPos().getY()) - this->box.getSize().getY()/2;
			float yMaxB = g->getBox().getPos().getY() + g->getBox().getSize().getY()/2;
			float yMaxA = (this->box.getPos().getY()) + this->box.getSize().getY()/2;
			float yMinB = g->getBox().getPos().getY() - g->getBox().getSize().getY()/2;

			//bool variables for each side
			bool checkLeft = false;
			bool checkRight = false;
			bool checkBottom = false;
			bool checkTop = false;
			//response variables to know how much to respond
			float leftResponse = xMaxB - xMinA;
			float rightResponse = xMaxA - xMinB;
			float topResponse = yMaxA - yMinB;
			float bottomResponse = yMaxB - yMinA;

			//check which side is being collided
			if(xMinA <= xMaxB && xMinA >= xMinB)
				checkLeft = true;
			if(xMaxA >= xMinB && xMaxA <= xMaxB)
				checkRight = true;
			if(yMinA <= yMaxB && yMinA >= yMinB)
				checkBottom = true;
			if(yMaxA >= yMinB && yMaxA <= yMaxB)
				checkTop = true;
			// check left and if true respond accordingly
			if(checkLeft && checkBottom && checkTop || checkLeft && checkBottom  && (leftResponse < bottomResponse)
				|| checkLeft && checkTop && (leftResponse < topResponse)){  
				this->pos.setX(this->getPos().getX() + (leftResponse/2));
				this->getVel().setX(this->getVel().getX() -1 * deltaT);
				return true;
			}
			// check right and if true respond accordingly
			if(checkRight && checkBottom && checkTop || checkRight && checkBottom && (rightResponse < bottomResponse)
				|| checkRight && checkTop && (rightResponse < topResponse)){  
				this->pos.setX(this->getPos().getX() - (rightResponse /2));
				this->getVel().setX(this->getVel().getX() -1 * deltaT);
				return true;
			}
			//check bottom and if true respond accordingly
			if(checkBottom && checkRight && (bottomResponse < rightResponse)|| checkBottom && checkLeft && (bottomResponse < leftResponse)
				|| checkBottom && checkRight && checkLeft){ 
				this->pos.setY(this->getPos().getY() + (bottomResponse + 0.01));
				if(this->getVel().getY() < -1){
					this->setVelY(-this->getVel().getY() * 0.3);
				}
				this->setJump(false);
				return true;
			}
			//check top and if true respond accordingly
			if(checkTop && checkRight && (topResponse < rightResponse)|| checkTop && checkLeft && (topResponse < bottomResponse)
				||checkTop && checkRight && checkLeft){ 
				this->pos.setY(this->getPos().getY() - (topResponse + 0.01));
				this->setVelY(0.0f);
				return true;
			}
			if(!checkTop && !checkBottom && !checkRight && !checkLeft){
				this->setJump(true);
			}
		}
		
		//return the result boolean
		return collision;
		
	}

	//method to check for collisions with a given gameobject
	bool Player::checkForCollisions(MovingPlatforms* g, double deltaT, double prevDeltaT){
		//bool variable to store results
		bool collision = false;
		//bool variable for if the circles are colliding
		bool circleTouch = false;
		//get the 2 bounding circles positions and radius
		float x1 = this->circle.getPos().getX();
		float x2 = g->getCircle().getPos().getX();
		float y1 = this->circle.getPos().getY();
		float y2 = g->getCircle().getPos().getY();
		float r1 = this->circle.getSize().getX();
		float r2 = g->getCircle().getSize().getX();

		//Check circles first
		if ((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) < (r1 + r2)*(r1 + r2)){
			circleTouch = true;
		}

		//if circles collided do more thorough checking
		if (circleTouch){
			//get the 2 bounding boxes verticies 
			float xMinA = (this->box.getPos().getX()/*+ this->vel.getX()*/) - this->box.getSize().getX() / 2;
			float xMaxB = g->getBox().getPos().getX() + g->getBox().getSize().getX() / 2;
			float xMaxA = (this->box.getPos().getX()/*+ this->vel.getX()*/) + this->box.getSize().getX() / 2;
			float xMinB = g->getBox().getPos().getX() - g->getBox().getSize().getX() / 2;
			float yMinA = (this->box.getPos().getY()/*+ this->vel.getY()*/) - this->box.getSize().getY() / 2;
			float yMaxB = g->getBox().getPos().getY() + g->getBox().getSize().getY() / 2;
			float yMaxA = (this->box.getPos().getY()/*+ this->vel.getY()*/) + this->box.getSize().getY() / 2;
			float yMinB = g->getBox().getPos().getY() - g->getBox().getSize().getY() / 2;

			//bool variables for each side
			bool checkLeft = false;
			bool checkRight = false;
			bool checkBottom = false;
			bool checkTop = false;
			//response variables to know how much to respond
			float leftResponse = xMaxB - xMinA;
			float rightResponse = xMaxA - xMinB;
			float topResponse = yMaxA - yMinB;
			float bottomResponse = yMaxB - yMinA;

			//check which side is being collided
			if (xMinA <= xMaxB && xMinA >= xMinB)
				checkLeft = true;
			if (xMaxA >= xMinB && xMaxA <= xMaxB)
				checkRight = true;
			if (yMinA <= yMaxB && yMinA >= yMinB)
				checkBottom = true;
			if (yMaxA >= yMinB && yMaxA <= yMaxB)
				checkTop = true;
			// check left and if true respond accordingly
			if (checkLeft && checkBottom && checkTop || checkLeft && checkBottom && (leftResponse < bottomResponse)
				|| checkLeft && checkTop && (leftResponse < topResponse)){
				this->pos.setX(this->getPos().getX() + (leftResponse + 0.1));
				this->getVel().setX(this->getVel().getX() - 1 * deltaT);
				return true;
			}
			// check right and if true respond accordingly
			if (checkRight && checkBottom && checkTop || checkRight && checkBottom && (rightResponse < bottomResponse)
				|| checkRight && checkTop && (rightResponse < topResponse)){
				this->pos.setX(this->getPos().getX() + (rightResponse - 0.1));
				this->getVel().setX(this->getVel().getX() - 1 * deltaT);
				return true;
			}
			//check bottom and if true respond accordingly
			if (checkBottom && checkRight && (bottomResponse < rightResponse) || checkBottom && checkLeft && (bottomResponse < leftResponse)
				|| checkBottom && checkRight && checkLeft){
				this->pos.setY(this->getPos().getY() + (bottomResponse + 0.01));
				this->pos.setX((pos.getX() + g->getVel().getX() / 10 * deltaT));
				if(this->getVel().getY() < -1){
					this->setVelY(-5);
				}
				this->setJump(false);
				return true;
			}
			//check top and if true respond accordingly
			if (checkTop && checkRight && (topResponse < rightResponse) || checkTop && checkLeft && (topResponse < bottomResponse)
				|| checkTop && checkRight && checkLeft){
				this->pos.setY(this->getPos().getY() - (topResponse + 0.01));
				this->setVelY(0.0f);
				return true;
			}
			if (!checkTop && !checkBottom && !checkRight && !checkLeft){
				this->setJump(true);
			}
		}

		//return the result boolean
		return collision;

	}

	//method to check for collisions with a given death object
	bool Player::checkForCollisions(Traps* g, double deltaT, double prevDeltaT){
		//bool variable to store results
		bool collision = false;
		//bool variable for if the circles are colliding
		bool circleTouch = false;
		//get the 2 bounding circles positions and radius
		float x1 = this->circle.getPos().getX();
		float x2 =  g->getCircle().getPos().getX();
		float y1 = this->circle.getPos().getY();
		float y2 = g->getCircle().getPos().getY();
		float r1 = this->circle.getSize().getX();
		float r2 = g->getCircle().getSize().getX();

		//Check circles first
		if((x1 - x2)*(x1-x2) + (y1-y2)*(y1-y2) < (r1+r2)*(r1+r2)){
			circleTouch = true;
		}

		//if circles collided do more thorough checking
		if(circleTouch){
			//get the 2 bounding boxes verticies 
			float xMinA = (this->box.getPos().getX()/*+ this->vel.getX()*/) - this->box.getSize().getX()/2;
			float xMaxB = g->getBox().getPos().getX() + g->getBox().getSize().getX()/2;
			float xMaxA = (this->box.getPos().getX()/*+ this->vel.getX()*/) + this->box.getSize().getX()/2;
			float xMinB = g->getBox().getPos().getX() - g->getBox().getSize().getX()/2;
			float yMinA = (this->box.getPos().getY()/*+ this->vel.getY()*/) - this->box.getSize().getY()/2;
			float yMaxB = g->getBox().getPos().getY() + g->getBox().getSize().getY()/2;
			float yMaxA = (this->box.getPos().getY()/*+ this->vel.getY()*/) + this->box.getSize().getY()/2;
			float yMinB = g->getBox().getPos().getY() - g->getBox().getSize().getY()/2;

			//bool variables for each side
			bool checkLeft = false;
			bool checkRight = false;
			bool checkBottom = false;
			bool checkTop = false;
			//response variables to know how much to respond
			float leftResponse = xMaxB - xMinA;
			float rightResponse = xMaxA - xMinB;
			float topResponse = yMaxA - yMinB;
			float bottomResponse = yMaxB - yMinA;

			//check which side is being collided
			if(xMinA <= xMaxB && xMinA >= xMinB)
				checkLeft = true;
			if(xMaxA >= xMinB && xMaxA <= xMaxB)
				checkRight = true;
			if(yMinA <= yMaxB && yMinA >= yMinB)
				checkBottom = true;
			if(yMaxA >= yMinB && yMaxA <= yMaxB)
				checkTop = true;
			// check left and if true respond accordingly
			if(checkLeft && checkBottom && checkTop || checkLeft && checkBottom  && (leftResponse < bottomResponse)
				|| checkLeft && checkTop && (leftResponse < topResponse)){  
				this->pos.setX(this->getPos().getX() + (leftResponse + 0.1));
				this->getVel().setX(this->getVel().getX() -1 * deltaT);
				return false;
			}
			// check right and if true respond accordingly
			if(checkRight && checkBottom && checkTop || checkRight && checkBottom && (rightResponse < bottomResponse)
				|| checkRight && checkTop && (rightResponse < topResponse)){  
				this->pos.setX(this->getPos().getX() + (rightResponse - 0.1));
				this->getVel().setX(this->getVel().getX() -1 * deltaT);
				return false;
			}
			//check bottom and if true respond accordingly
			if(checkBottom && checkRight && (bottomResponse < rightResponse)|| checkBottom && checkLeft && (bottomResponse < leftResponse)
				|| checkBottom && checkRight && checkLeft){  
				this->pos.setY(this->getPos().getY() + bottomResponse);
				this->setVelY(-this->getVel().getY() * 0.3);
				this->setJump(false);
				this->setDead();
				return true;
			}
			//check top and if true respond accordingly
			if(checkTop && checkRight && (topResponse < rightResponse)|| checkTop && checkLeft && (topResponse < bottomResponse)
				|| checkTop && checkRight && checkLeft){ 
				this->pos.setY(this->getPos().getY() - (topResponse + 0.2));
				this->setVelY(-5.0f);
				return false;
			}
			if(!checkTop && !checkBottom && !checkRight && !checkLeft){
					this->setJump(true);
			}
		}
		
		//return the result boolean
		return collision;
		
	}
	
	
	//set method for jump
	void Player::setJump(bool jump){
		this->jump = jump;
	}
	//getter for jump
	bool Player::getJump(){
		return this->jump;
	}

	vector<GLuint> Player::getMovingTextures(){
		return this->moving_textures;
	}
	void Player::addMovingTexture(GLuint texture){
		this->moving_textures.push_back(texture);
	}

	vector<GLuint> Player::getJumpingTextures(){
		return this->jumping_textures;
	}
	void Player::addJumpingTexture(GLuint texture){
		this->jumping_textures.push_back(texture);
	}

	void Player::setTextures(){
		//idle
		GLuint idle1 = loadPNG("Player_textures/idle/frame-1.png");
		GLuint idle2 = loadPNG("Player_textures/idle/frame-2.png");

		this->addIdleTexture(idle1);
		this->addIdleTexture(idle2);

		//Running
		GLuint run1 = loadPNG("Player_textures/running/frame-1.png");
		GLuint run2 = loadPNG("Player_textures/running/frame-2.png");
		GLuint run3 = loadPNG("Player_textures/running/frame-3.png");
		GLuint run4 = loadPNG("Player_textures/running/frame-4.png");
		GLuint run5 = loadPNG("Player_textures/running/frame-5.png");
		GLuint run6 = loadPNG("Player_textures/running/frame-6.png");

		this->addMovingTexture(run1);
		this->addMovingTexture(run2);
		this->addMovingTexture(run3);
		this->addMovingTexture(run4);
		this->addMovingTexture(run5);
		this->addMovingTexture(run6);

		//Jumping
		GLuint fall = loadPNG("Player_textures/jump/jump_fall.png");
		GLuint jump = loadPNG("Player_textures/jump/jump_up.png");

		this->addJumpingTexture(fall);
		this->addJumpingTexture(jump);

	}

	void Player::setDead(){
		this->isDead = true;
	}

	bool Player::checkLife(){
		return this->isDead;
	}
