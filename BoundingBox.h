#pragma once

#include "CollidableObjects.h"
#include <Windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library

class BoundingBox : public CollidableObjects{

public:
	//constructors
	BoundingBox();
	BoundingBox(Vec2 pos, Vec2 size);
	//equality operator
	BoundingBox operator = (const BoundingBox &v);
	//draw method
	void draw();
	//update method
	void update(Vec2 attachedNewPos);
};
