#include "Traps.h"

Traps::Traps(Vec2 ipos, Vec2 isize){
	this->pos = ipos;
	this->size = isize;
}

Traps::Traps(float x, float y, float width, float height){
	Vec2 pos = Vec2(x, y);
	Vec2 size = Vec2(width, height);
	this->pos = pos;
	this->size = size;
}

void Traps::setTextures(char* file){
	textureFile = file;
	GLuint idle1 = loadPNG(textureFile);
	this->addIdleTexture(idle1);
}