#include "ScoreScreen.h"

#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE

ScoreScreen::ScoreScreen(OpenGLApplication *app)
	:Activity(app)
{frames = 0;}

void ScoreScreen::initialise(){											// Called on application start up

	int level = 0;
	int time = 0;
	int deaths = 0;
	

	ifstream dump("dump.txt");
	//get the line
	string dumpline;
	//iterate over the lines in the file
	while (getline(dump, dumpline)){

		string temp;

		//get the level number
		temp = dumpline.substr(0, dumpline.find(','));
		//convert it to int
		stringstream(temp) >> level;
		dumpline.erase(0, dumpline.find(",") + 1);

		//get the time left
		temp = dumpline.substr(0, dumpline.find(','));
		//convert pages to int
		stringstream(temp) >> time;
		dumpline.erase(0, dumpline.find(",") + 1);

		//get the number of deaths
		temp = dumpline.substr(0, dumpline.find(','));
		//convert pages to int
		stringstream(temp) >> deaths;
	}

	dump.close();

	//level score is the time left in seconds * 100 minus the number of deaths * 50
	score = (time * 100);
	score = score - (deaths * 50);

	int tempScore = score;

	tensTho = tempScore / 10000;
	if (tensTho < 0)
		tensTho = 0;
	tempScore = tempScore % 10000;
	tho = tempScore / 1000;
	if (tho < 0)
		tho = 0;
	tempScore = tempScore % 1000;
	hun = tempScore / 100;
	if (hun < 0)
		hun = 0;
	tempScore = tempScore % 100;
	tens = tempScore / 10;
	if (tens < 0)
		tens = 0;
	tempScore = tempScore % 10;
	units = tempScore;
	if (units < 0)
		units = 0;

	if(level == 1){
		//check to see if its a high score
		ifstream highscore("highscore1.txt");
		//get the line
		string line;
		//iterate over the lines in the file
		while (getline(highscore, line)){

			string temp;
			int highScoreLevel, currentHigh;
			temp = line.substr(0, line.find('\n'));
			//convert highscore to int
			stringstream(temp) >> currentHigh;

			//check to see if the new score is higher and if it is replace it with the new score
			if (currentHigh < score){
				highscore.close();
				ofstream newHighscore;
				newHighscore.open("highscore1.txt");
				newHighscore << score << endl;
				newHighscore.close();
			}
			else{
				newHighscore = false;
				highscore.close();
			}
		}
	}
	else if(level == 2){
		//check to see if its a high score
		ifstream highscore("highscore2.txt");
		//get the line
		string line;
		//iterate over the lines in the file
		while (getline(highscore, line)){

			string temp;
			int highScoreLevel, currentHigh;
			temp = line.substr(0, line.find('\n'));
			//convert highscore to int
			stringstream(temp) >> currentHigh;

			//check to see if the new score is higher and if it is replace it with the new score
			if (currentHigh < score){
				highscore.close();
				ofstream newHighscore;
				if(level == 2)
				newHighscore.open("highscore2.txt");
				newHighscore << score << endl;
				newHighscore.close();
			}
			else{
				newHighscore = false;
				highscore.close();
			}
		}
	}

	LoadPNG imageloader = LoadPNG();
	background = imageloader.loadPNG("Background_textures/Full-background.png");
	scoreTex = imageloader.loadPNG("Background_textures/Score.png");
	

	//numbers
	GLuint temp = imageloader.loadPNG("Background_textures/zero.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/one.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/two.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/three.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/four.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/five.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/six.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/seven.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/eight.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/nine.png");
	numbers.push_back(temp);

	
	temp = imageloader.loadPNG("Background_textures/Highscore.png");
	highscoreTexs.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/Highscore-select.png");
	highscoreTexs.push_back(temp);


	Button* menu = new Button(0, -10, 20, 5, "Background_textures/Menu.png", "Background_textures/Menu-select.png", 1);
	buttons.push_back(menu);

}
void ScoreScreen::shutdown(){											// Called on application shut down
	numbers.clear();
	buttons.clear();
}
void ScoreScreen::finish(){												// Called when the activity is finished and needs to be rebuilt from scratch

}

void ScoreScreen::onSwitchIn(){											// Activity switch in; called when the activity changes and this one switches in

}
void ScoreScreen::onSwitchOut(){


}
void ScoreScreen::onReshape(int w, int h){						// called when the window is resized
	glViewport(0, 0, w, h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();;

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect, -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();
}
void ScoreScreen::update(double deltaT, double prevDeltaT){				// Update the application;
	for each(Button* b in buttons){
		b->setCollidables();
		b->update(deltaT, prevDeltaT);
	}

	if (newHighscore){
		frames += deltaT;
		int temp = (int) frames;
		if (temp > highscoreTexs.size() - 1){
			frames = 0;
			temp = 0;
		}
		highscoreTex = highscoreTexs.at(temp);
	}
}
void ScoreScreen::draw(){	
	// OpenGL render calls go in this method
	double aspect = app->getAspectRatio();
	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, background);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
		glEnd();

	//pop the matrix
	glPopMatrix();
	

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, scoreTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-5, 7);
			glTexCoord2f(0, 1); glVertex2f(-5, 10);
			glTexCoord2f(1, 1); glVertex2f(5, 10);
			glTexCoord2f(1, 0); glVertex2f(5, 7);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tensTho));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-5, 0);
			glTexCoord2f(0, 1); glVertex2f(-5, 6);
			glTexCoord2f(1, 1); glVertex2f(-3, 6);
			glTexCoord2f(1, 0); glVertex2f(-3, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tho));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-3, 0);
			glTexCoord2f(0, 1); glVertex2f(-3, 6);
			glTexCoord2f(1, 1); glVertex2f(-1, 6);
			glTexCoord2f(1, 0); glVertex2f(-1, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(hun));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-1, 0);
			glTexCoord2f(0, 1); glVertex2f(-1, 6);
			glTexCoord2f(1, 1); glVertex2f(1, 6);
			glTexCoord2f(1, 0); glVertex2f(1, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tens));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(1, 0);
			glTexCoord2f(0, 1); glVertex2f(1, 6);
			glTexCoord2f(1, 1); glVertex2f(3, 6);
			glTexCoord2f(1, 0); glVertex2f(3, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(units));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(3, 0);
			glTexCoord2f(0, 1); glVertex2f(3, 6);
			glTexCoord2f(1, 1); glVertex2f(5, 6);
			glTexCoord2f(1, 0); glVertex2f(5, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();
	if (newHighscore){
		glPushMatrix();
			// Identity matrix
			glLoadIdentity();

			// Bind our start screen texture to GL_TEXTURE_2D
			glBindTexture(GL_TEXTURE_2D, highscoreTex);
			// Enable 2D texturing
			glEnable(GL_TEXTURE_2D);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			//make a square, with texture co-ordinates for each vertex
			glBegin(GL_POLYGON);
				glTexCoord2f(0, 0); glVertex2f(-5, -4);
				glTexCoord2f(0, 1); glVertex2f(-5, -1);
				glTexCoord2f(1, 1); glVertex2f(5, -1);
				glTexCoord2f(1, 0); glVertex2f(5, -4);
			glEnd();

		//pop the matrix
		glPopMatrix();
	}

	for each(Button* b in buttons){
		b->draw();
	}
	glFlush();

}

// Event handler methods; these methods are called in response to user actions:
void ScoreScreen::onKeyDown(int key){									// Called when key pressed

}
void ScoreScreen::onKeyUp(int key){									// Called when key released

}
void ScoreScreen::onMouseDown(int button, float mouseX, float mouseY){		// Called when mouse button pressed

}
void ScoreScreen::onMouseUp(int button, float mouseX, float mouseY){			// Called when mouse button released
	for (int i = 0; i < buttons.size(); i++){
		Button* b = buttons.at(i);
		if (b->isSelected() && b->getID() == 1 && button == 0){
			shutdown();
			app->swicthActivity(app->mainMenu);
		}
	}
}
void ScoreScreen::onMouseMove(float mouseX, float mouseY){					// Called when mouse moved
	double aspect = app->getAspectRatio();
	double oriAspect = app->getOriAspectRatio();
	float x = ((mouseX - app->getScreenWidth() / 2) / (VIEW_SIZE*0.5*oriAspect*(oriAspect / aspect) * ((double)app->getScreenWidth() / (double)app->getOriScreenWidth())));
	float y = ((app->getScreenHeight() / 2 - mouseY) / (VIEW_SIZE*0.5*oriAspect* ((double)app->getScreenHeight() / (double)app->getOriScreenHeight())));
	for each(Button* b in buttons){
		b->checkForCollisions(x, y);
	}
}