#pragma once

#include "BoundingCircle.h"
#include <math.h>

//constant PI 
float const PI = 3.14159265359;

//default constructor for bounding circle
BoundingCircle::BoundingCircle(){}
//constructor for bounding circle using vec2 and a float
BoundingCircle::BoundingCircle(Vec2 pos, float radius){
	this->pos = pos;
	this->size.setX(radius);
}
//operator overload to set the bounding circle to another circle
BoundingCircle BoundingCircle::operator = (const BoundingCircle &v){
	this->pos = v.pos;
	this->size = v.size;
	return *this;
}

//method to draw the bounding circle
void BoundingCircle::draw(){
	//push matrix
	glPushMatrix();
	//colour the circle (debugging)
	glColor3f(0.0f, 1.0f, 1.0f);
	//draw the circle using a line loop and 360 verticies
	glBegin(GL_LINE_LOOP);
		for (int i = 0; i < 360; i += 5){
			float xcoord = this->pos.getX() + this->size.getX() * cos(i*(PI / 180));
			float ycoord = this->pos.getY() + this->size.getX() * sin(i*(PI / 180));
			glVertex2f(xcoord, ycoord);
		}
	glEnd();
	//pop matrix
	glPopMatrix();
}
//update the bounding circle using a new given positions vec2
void BoundingCircle::update(Vec2 attachedNewPos){

	this->pos.setX(attachedNewPos.getX());
	this->pos.setY(attachedNewPos.getY());
}