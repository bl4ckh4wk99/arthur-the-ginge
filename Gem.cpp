#include "Gem.h"

Gem::Gem(Vec2 pos){
	this->pos = pos;
	this->size = Vec2(2, 2);
	collected = false;
	setTextures();
	setCollidables();
}

Gem::Gem(float x, float y){
	Vec2 pos = Vec2(x, y);
	this->pos = pos;
	this->size = Vec2(2, 2);
	collected = false;
	setTextures();
	setCollidables();
}

bool Gem::checkCollected(){
	return this->collected;
}



//draw method
void Gem::draw(){
	if (!collected){
		//push the matrix
		glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
		glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
		glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() - this->size.getY() / 2);
		glEnd();
		//pop the matrix
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		//circle.draw();

	}
}
//update method
void Gem::update(double deltaT, double prevDeltaT){}

//set collidables for this object
void Gem::setCollidables(){
	//create a bounding circle
	float radius = pow((this->size.getX() / 2), 2) + pow((this->size.getY() / 2), 2);
	radius = sqrt(radius);
	BoundingCircle circle = BoundingCircle(this->pos, radius);
	this->circle = circle;
}

bool Gem::checkForCollisions(Player* g, double deltaT, double prevDeltaT){
	//bool variable for if the circles are colliding
	bool circleTouch = false;
	//get the 2 bounding circles positions and radius
	float x1 = this->circle.getPos().getX();
	float x2 = g->getCircle().getPos().getX();
	float y1 = this->circle.getPos().getY();
	float y2 = g->getCircle().getPos().getY();
	float r1 = this->circle.getSize().getX();
	float r2 = g->getCircle().getSize().getX();

	//Check circles first
	if ((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) < (r1 + r2)*(r1 + r2)){
		circleTouch = true;
		this->collected = true;
	}

	return circleTouch;
}


void Gem::setTextures(){
	currentTexture = loadPNG("World_textures/gem.png");
}
