//
// This is taken from the Graphics 1 OpenGL template
//

/*
Activity base class implementation

For more information on the structure of the template, please see README.txt
*/

#include "OpenGLApplication.h"

#include "Activity.h"



Activity::Activity(OpenGLApplication *app)
{
	this->input = app->getInputState();
	this->app = app;
}


/*
*
* ACTIVITY METHODS
*
*/
void Activity::initialise()
{}

void Activity::shutdown()
{}

void Activity::onSwitchIn()
{}

void Activity::onSwitchOut()
{}

void Activity::finish()
{}

void Activity::onReshape(int width, int height)
{}

void Activity::update(double deltaT, double prevDeltaT)
{}

void Activity::draw()
{}