//
// This is taken from the Graphics 1 OpenGL template
//

#include <stdio.h>

#include <Windows.h>

#include "MainMenu.h"
#include "LevelOne.h"
#include "LevelTwo.h"
#include "DeathScreen.h"
#include "Controls.h"
#include "ScoreScreen.h"
#include "LevelSelect.h"
#include "OutOfTime.h"

#include <iostream>
#include <fstream>

#include "OpenGLApplication.h"

OpenGLApplication::OpenGLApplication()
{
	// Sensible defaults to start with
	screenWidth = 800;
	oriScreenWidth = screenWidth;
	screenHeight = 600;
	oriScreenHeight = screenHeight;

	currentActivity = NULL;

	finished = false;
}


// Getters for input state and screen size
Inputs * OpenGLApplication::getInputState()
{
	return &input;
}


int OpenGLApplication::getScreenWidth() const
{
	return screenWidth;
}

int OpenGLApplication::getScreenHeight() const
{
	return screenHeight;
}

int OpenGLApplication::getOriScreenWidth() const
{
	return oriScreenWidth;
}
int OpenGLApplication::getOriScreenHeight() const
{
	return oriScreenHeight;
}

double OpenGLApplication::getAspectRatio() const
{
	return (double)screenWidth / (double)screenHeight;
}

double OpenGLApplication::getOriAspectRatio() const
{
	return (double)oriScreenWidth / (double)oriScreenHeight;
}



// Activity methods
Activity * OpenGLApplication::getCurrentActivity()
{
	return currentActivity;
}

void OpenGLApplication::swicthActivity(Activity *a)
{
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchOut();
	}
	currentActivity = a;

	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}

void OpenGLApplication::playerDied(){
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchOut();
	}
	previousActivity = currentActivity;
	currentActivity = death;
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}

void OpenGLApplication::playerRespawn(){
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchOut();
	}
	currentActivity = previousActivity;
	previousActivity = death;
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}

void OpenGLApplication::timeRanOut(){
	if (currentActivity != NULL)
	{
		currentActivity->finish();
	}
	previousActivity = currentActivity;
	currentActivity = time;
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}

void OpenGLApplication::finishLevel(Hud *hud, int levelNum){
	if (currentActivity != NULL)
	{
		currentActivity->finish();
	}
	previousActivity = currentActivity;
	ofstream dump;
	dump.open("dump.txt");
	dump << levelNum << "," << hud->getTimeInSec() << "," << hud->getNumDeaths() << endl;
	dump.close();

	scoreScreen->initialise();
	mainMenu->initialise();
	controls->initialise();
	levelSelect->initialise();
	currentActivity = scoreScreen;

	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}

}

void OpenGLApplication::startLevel(Activity* a){
	if (currentActivity != NULL)
	{
		currentActivity->finish();
	}
	a->initialise();
	mainMenu->shutdown();
	controls->shutdown();
	levelSelect->shutdown();
	currentActivity = a;
	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}

void OpenGLApplication::restartLevel(){
	if (currentActivity != NULL)
	{
		currentActivity->finish();
	}
	currentActivity = previousActivity;
	previousActivity = time;
	if (currentActivity != NULL)
	{
		currentActivity->initialise();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}

void OpenGLApplication::exitLevel(){
	if (currentActivity != NULL)
	{
		currentActivity->finish();
	}
	
	mainMenu->initialise();
	controls->initialise();
	levelSelect->initialise();
	currentActivity = mainMenu;

	if (currentActivity != NULL)
	{
		currentActivity->onSwitchIn();
		currentActivity->onReshape(screenWidth, screenHeight);
	}
}




// Initialise application
void OpenGLApplication::initialise()
{
	mainMenu = new MainMenu(this);
	levelSelect = new LevelSelect(this);
	controls = new Controls(this);
	
	levelOne = new LevelOne(this);
	levelTwo = new LevelTwo(this);
	death = new DeathScreen(this);
	time = new OutOfTime(this);
	scoreScreen = new ScoreScreen(this);


	mainMenu->initialise();
	levelSelect->initialise();
	controls->initialise();
	currentActivity = mainMenu;
}

// Shutdown application
void OpenGLApplication::shutdown()
{
	mainMenu->shutdown();
	levelOne->shutdown();
	levelTwo->shutdown();
	death->shutdown();
	time->shutdown();
	controls->shutdown();
	scoreScreen->shutdown();

	delete mainMenu;
	delete levelOne;
	delete levelTwo;
	delete death;
	delete controls;
	delete scoreScreen;
	delete time;

	mainMenu = NULL;
	levelOne = NULL;
	levelTwo = NULL;
	controls = NULL;
	scoreScreen = NULL;
	death = NULL;
	time = NULL;
}



void OpenGLApplication::finish()
{
	finished = true;
}

bool OpenGLApplication::isApplicationFinished()
{
	return finished;
}



/*
*
* The methods below are invoked by the Win32 code to inform the application of various events
*
*/

void OpenGLApplication::handleReshape(int width, int height)
{

	screenWidth = width;
	screenHeight = height;

	if (currentActivity != NULL)
	{
		currentActivity->onReshape(width, height);
	}
}

void OpenGLApplication::handleUpdate(double deltaT, double prevDeltaT)
{
	// Just call update
	if (currentActivity != NULL)
	{
		currentActivity->update(deltaT, prevDeltaT);
	}
}

void OpenGLApplication::handleDraw()
{

	if (currentActivity != NULL)
	{
		currentActivity->draw();
	}
}

//********************************INPUT FUNCTIONS*****************************************************

void OpenGLApplication::handleMouseDown(int button, float mouseX, float mouseY)
{

	// Tell inputState, then invoke the event method
	input.handleMouseDown(button, mouseX, mouseY);
	if (currentActivity != NULL)
	{
		currentActivity->onMouseDown(button, mouseX, mouseY);
	}
}

void OpenGLApplication::handleMouseUp(int button, float mouseX, float mouseY)
{
	// You DO NOT need to write any additional code here...

	// Tell inputState, then invoke the event method
	input.handleMouseUp(button, mouseX, mouseY);
	if (currentActivity != NULL)
	{
		currentActivity->onMouseUp(button, mouseX, mouseY);
	}
}

void OpenGLApplication::handleMouseMove(float mouseX, float mouseY)
{
	// You DO NOT need to write any additional code here...

	// Tell inputState, then invoke the event method
	input.handleMouseMove(mouseX, mouseY);
	if (currentActivity != NULL)
	{
		currentActivity->onMouseMove(mouseX, mouseY);
	}
}

void OpenGLApplication::handleKeyDown(int key)
{
	// You DO NOT need to write any additional code here...

	// Tell inputState, then invoke the event method
	input.handleKeyDown(key);
	if (currentActivity != NULL)
	{
		currentActivity->onKeyDown(key);
	}

	// Finish if the escape key has been pressed
	if (key == VK_ESCAPE)
	{
		finish();
	}
}

void OpenGLApplication::handleKeyUp(int key)
{
	// You DO NOT need to write any additional code here...

	// Tell inputState, then invoke the event method
	input.handleKeyUp(key);
	if (currentActivity != NULL)
	{
		currentActivity->onKeyUp(key);
	}
}



