#include "Hud.h"

Hud::Hud(int time){
	this->pos = Vec2();
	this->size = Vec2();

	//Clock
	this->time = (float)time;

	this->tensM = (int)(time / 60) / 10;
	this->M = (int)(time / 60) % 10;
	time = time % 60;
	this->tensS = (int)(time / 10);
	this->S = (int)(time % 10);

	//Death Counter
	this->counter = 0;

	//Gem Counter
	this->gems = 0;
}


void Hud::draw(){
	//***********************************************************************************
	//Clock
	//***********************************************************************************
	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, Min);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() + 16 - (5 / 2 - 0.5), this->pos.getY() + 12);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() + 16 - (5 / 2 - 0.5), this->pos.getY() + 14);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + 16 - (5 / 4 - 0.5), this->pos.getY() + 14);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + 16 - (5 / 4 - 0.5), this->pos.getY() + 12);
		glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, Min);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() + 16 - (5 / 4 - 0.5), this->pos.getY() + 12);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() + 16 - (5 / 4 - 0.5), this->pos.getY() + 14);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + 16 - 0.5, this->pos.getY() + 14);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + 16 - 0.5, this->pos.getY() + 12);
		glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
		glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, colon);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() + 16 - 0.5, this->pos.getY() + 12);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() + 16 - 0.5, this->pos.getY() + 14);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + 16 + 0.5, this->pos.getY() + 14);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + 16 + 0.5, this->pos.getY() + 12);
		glEnd();
	//pop the matrix
	glPopMatrix();


	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, tensSec);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX()+ 16 + 0.5, this->pos.getY() + 12);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX()+ 16 + 0.5, this->pos.getY() + 14);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX()+ 16 + (5 / 4 + 0.5), this->pos.getY() + 14);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX()+ 16 + (5 / 4 + 0.5), this->pos.getY() + 12);
		glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
		glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, Sec);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() + 16 + (5 / 4 + 0.5), this->pos.getY() + 12);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() + 16 + (5 / 4 + 0.5), this->pos.getY() + 14);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + 16 + (5 / 2 + 0.5), this->pos.getY() + 14);
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + 16 + (5 / 2 + 0.5), this->pos.getY() + 12);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	//***********************************************************************************
	//Death Counter
	//***********************************************************************************
	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, deathSymbol);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - 16 - (5/ 2), this->pos.getY() + 13 - (2 / 2) );
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - 16 - (5 / 2), this->pos.getY() + 13 + (2 / 2));
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() - 16 , this->pos.getY() + 13 + (2 / 2));
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() - 16 , this->pos.getY() + 13 - (2 / 2));
		glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, tens);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - 16, this->pos.getY() + 13 - (2 / 2));
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - 16, this->pos.getY() + 13 + (2 / 2));
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() - 16 + (5 / 4), this->pos.getY() + 13 + (2 / 2));
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() - 16 + (5 / 4), this->pos.getY() + 13 - (2 / 2));
		glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, units);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - 16 + (5 / 4), this->pos.getY() + 13 - (2 / 2));
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - 16 + (5 / 4), this->pos.getY() + 13 + (2 / 2));
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() - 16 + (5 / 2), this->pos.getY() + 13 + (2 / 2));
			glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() - 16 + (5 / 2), this->pos.getY() + 13 - (2 / 2));
		glEnd();
	//pop the matrix
	glPopMatrix();
	//***********************************************************************************
	//Gem Counter
	//***********************************************************************************
	//push the matrix
	glPushMatrix();
	//start textures
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, gemCounter);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw the object using its size and positions variables
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() - (5 / 2), this->pos.getY() + 13 - (2 / 2));
	glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - (5 / 2), this->pos.getY() + 13 + (2 / 2));
	glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX(), this->pos.getY() + 13 + (2 / 2));
	glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX(), this->pos.getY() + 13 - (2 / 2));
	glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
	glPushMatrix();
	//start textures
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, gTens);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw the object using its size and positions variables
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX(), this->pos.getY() + 13 - (2 / 2));
	glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX(), this->pos.getY() + 13 + (2 / 2));
	glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + (5 / 4), this->pos.getY() + 13 + (2 / 2));
	glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + (5 / 4), this->pos.getY() + 13 - (2 / 2));
	glEnd();
	//pop the matrix
	glPopMatrix();

	//push the matrix
	glPushMatrix();
	//start textures
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, gUnits);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw the object using its size and positions variables
	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0); glVertex2f(this->pos.getX() + (5 / 4), this->pos.getY() + 13 - (2 / 2));
	glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() + (5 / 4), this->pos.getY() + 13 + (2 / 2));
	glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + (5 / 2), this->pos.getY() + 13 + (2 / 2));
	glTexCoord2f(1.0, 0.0); glVertex2f(this->pos.getX() + (5 / 2), this->pos.getY() + 13 - (2 / 2));
	glEnd();
	//pop the matrix
	glPopMatrix();
}

void Hud::update(double deltaT, double prevDeltaT){
	//Clock
	this->time = this->time - deltaT;
	double temp = this->time;

	this->tensM = (int)(temp / 60) / 10;
	this->M = (int)(temp / 60) % 10;
	temp = ((int)temp % 60);
	this->tensS = (int)(temp / 10);
	this->S = (int)((int)temp % 10);

	if (tensM <= 0 && M <= 0 && tensS <= 0 && S <= 0){
		tensMin = numbers.at(0);
		Min = numbers.at(0);
		tensSec = numbers.at(0);
		Sec = numbers.at(0);
	}
	else{
		tensMin = numbers.at(tensM);
		Min = numbers.at(M);
		tensSec = numbers.at(tensS);
		Sec = numbers.at(S);
	}

	//Death Counter
	if (counter / 10 >= 9 && counter % 10 >= 9){
		tens = numbers.at(9);
		units = numbers.at(9);
	}
	else{
		tens = numbers.at(counter / 10);
		units = numbers.at(counter % 10);
	}
	//Gem Counter
	if (gems / 10 >= 9 && gems % 10 >= 9){
		gTens = numbers.at(9);
		gUnits = numbers.at(9);
	}
	else{
		gTens = numbers.at(gems / 10);
		gUnits = numbers.at(gems % 10);
	}
}

void Hud::updatePos(Camera* c){
	Vec2 cameraPos = c->getPos();
	this->pos.setX(cameraPos.getX());
	this->pos.setY(cameraPos.getY());

}


void Hud::setCollidables(){}

void Hud::setTextures(){
	//numbers
	GLuint temp = loadPNG("Background_textures/zero.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/one.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/two.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/three.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/four.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/five.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/six.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/seven.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/eight.png");
	numbers.push_back(temp);
	temp = loadPNG("Background_textures/nine.png");
	numbers.push_back(temp);

	//Clock
	this->colon = loadPNG("Background_textures/colon.png");

	//Death Counter
	this->deathSymbol = loadPNG("Background_textures/deathCounter.png");

	//Gem Counter
	this->gemCounter = loadPNG("Background_textures/GemCounter.png");
}


void Hud::incrementCounter(){
	this->counter++;
}

int Hud::getNumDeaths(){
	return this->counter;
}

void Hud::incrementGems(){
	this->gems++;
}

int Hud::getNumGems(){
	return this->gems;
}

int Hud::getTimeInSec(){
	int temp = this->S;
	temp += (10 * this->tensS);
	temp += (60 * this->M);
	temp += (60 * 10 * this->tensM);

	return temp;
}

bool Hud::checkTime(){
	if (getTimeInSec() <= 0){
		return false;
	}
	return true;
}

void Hud::gemCollected(){
	incrementGems();
	this->time += 10;
}