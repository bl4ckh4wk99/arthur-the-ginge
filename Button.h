#pragma once
#include "Gameobject.h"

class Button: public GameObject{
private:
	int ID;
	bool selected;

public:
	//constructors
	Button(Vec2 pos, Vec2 size, char* tex1, char* tex2, int ID);

	Button(float x, float y, float width, float height, char* tex1, char* tex2, int ID);
	//getter and setters for is selected and ID
	bool isSelected();
	void setSelected(bool i);

	int getID();
	//draw method
	void draw();
	//update method
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//set textures
	void setTextures();
	//check collisions
	bool checkForCollisions(float mouseX, float mouseY);

};