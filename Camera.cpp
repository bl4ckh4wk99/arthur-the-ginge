#include "Camera.h"

Camera::Camera(Vec2 ipos){
	this->pos = ipos;
}

Camera::Camera(float x, float y){
	Vec2 pos = Vec2(x, y);
	this->pos = pos;
}

void Camera::draw(){}

void Camera::updatePos(double deltaT, double prevDeltaT, Player* p){
	Vec2 playerPos = p->getPos();

	float xDiff = playerPos.getX() - this->pos.getX();
	float yDiff = playerPos.getY() - this->pos.getY();

	this->pos.setX(pos.getX() + (xDiff * 4 * deltaT));
	this->pos.setY(pos.getY() + (yDiff * 4 * deltaT));
}

void Camera::update(double deltaT, double prevDeltaT){}

void Camera::setCollidables(){}

void Camera::setTextures(){}