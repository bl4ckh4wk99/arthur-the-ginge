#pragma once
//
// This derived from the Graphics 1 OpenGL template
//

#include "Activity.h"
#include <vector>

class ControlActivity : public Activity{
private:
	// We need a texture ID for our start screen texture
	GLuint backgroundTex, menuTex, aKey, dKey,leftKey, rightKey, space, pKey, leftRight, jump, pause;
	vector<GLuint> menuTexs, aKeys, dKeys, leftKeys, rightKeys, spaces, pKeys;
public:
	ControlActivity(OpenGLApplication *app);

	// ACTIVITY METHODS
	virtual void initialise();											// Called on application start up
	virtual void finish();												// Called when the activity is finished and needs to be rebuilt from scratch
	virtual void shutdown();											// Called on application shut down

	virtual void onSwitchIn();											// Activity switch in; called when the activity changes and this one switches in
	virtual void onReshape(int width, int height);						// called when the window is resized
	virtual void draw();
	virtual void update(double deltaT, double prevDeltaT);				// Update the application; if the current frame is frame number F, then the previous frame is F-1 and the one before that is F-2

	// Event handler methods; these methods are called in response to user actions:
	void onMouseDown(int button, float mouseX, float mouseY);		// Called when mouse button pressed
	void onMouseUp(int button, float mouseX, float mouseY);			// Called when mouse button released
	void onMouseMove(float mouseX, float mouseY);					// Called when mouse moved
	void onKeyDown(int key);										// Called when key pressed
	void onKeyUp(int key);											// Called when key released

};