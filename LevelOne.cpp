//
// This is derrived from the Graphics 1 OpenGL template
//


//Level 1 implementation

#include <windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library


#include "OpenGLApplication.h"			// Needed to access member functions and variables from OpenGLApplication
#include "LevelOne.h"

#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE

//, hStart, hMid, hEnd, vStart, vMid, vEnd;

LevelOne::LevelOne(OpenGLApplication *app)
	: Activity(app)		// Call super constructor
{paused = false;}


void LevelOne::initialise()
{	
	
	

	onSwitchIn();

	hud = new Hud(120);
	hud->setTextures();

	LoadPNG imageLoader = LoadPNG();
	background1 = imageLoader.loadPNG("World_textures/background.png");
	background2 = imageLoader.loadPNG("World_textures/background2.png");
	background4 = imageLoader.loadPNG("World_textures/layer-1.png");
	background3 = imageLoader.loadPNG("World_textures/BrickBack.png");
	grassPlat = imageLoader.loadPNG("World_textures/Tile.png");
	dirt = imageLoader.loadPNG("World_textures/Brick.png");
	brickPlat = imageLoader.loadPNG("World_textures/BrickTile.png");
	brickBase = imageLoader.loadPNG("World_textures/Brick.png");

	Gem* gem1 = new Gem(60, 3);
	gems.push_back(gem1);

	Gem* gem2 = new Gem(76, 18);
	gems.push_back(gem2);

	Gem* gem3 = new Gem(145, 12);
	gems.push_back(gem3);

	Gem* gem4 = new Gem(145, -48);
	gems.push_back(gem4);

	Gem* gem5 = new Gem(123, -28);
	gems.push_back(gem5);

	Gem* gem6 = new Gem(55, -48);
	gems.push_back(gem6);

	Gem* gem7 = new Gem(20, -77);
	gems.push_back(gem7);

	Gem* gem8 = new Gem(65, -64);
	gems.push_back(gem8);

	Gem* gem9 = new Gem(83, -64);
	gems.push_back(gem9);

	Gem* gem10 = new Gem(150, -68);
	gems.push_back(gem10);
} 


void LevelOne::shutdown()
{	
	traps.clear();
	platformObjects.clear();
	enemies.clear();
	mPlatformObjects.clear();
	saws.clear();
	followers.clear();

	camera = NULL;
	player = NULL;
	exit = NULL;
	hud = NULL;
}



void LevelOne::onSwitchIn()
{

	glClearColor(0.0,0.0,0.0,0.0);						//sets the clear colour to black

	//**************************************************************
	//************************FILLER********************************
	//**************************************************************
	Platforms* filler1 = new Platforms(15, -6, 30, 8, brickBase);
	platformObjects.push_back(filler1);

	Platforms* filler2 = new Platforms(55, -6, 30, 8, brickBase);
	platformObjects.push_back(filler2);

	Platforms* filler3 = new Platforms(93, -6, 30, 8, brickBase);
	platformObjects.push_back(filler3);

	Platforms* filler4 = new Platforms(130, -6, 16, 8, brickBase);
	platformObjects.push_back(filler4);

	Platforms* filler5 = new Platforms(164, -6, 8, 8, brickBase);
	platformObjects.push_back(filler5);


	//********************************************************
	//Floor 1
	//********************************************************
	Platforms* floor1 = new Platforms(15, -1, 30, 2, grassPlat);
	platformObjects.push_back(floor1);

	Platforms* base1 = new Platforms(85, -9, 170, 2, dirt);
	platformObjects.push_back(base1);

	Traps* spikes = new Traps(35, -7, 10, 2);
	spikes->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes);

	Platforms* floor2 = new Platforms(55, -1, 30, 2, grassPlat);
	platformObjects.push_back(floor2);

	Platforms* starir1_1 = new Platforms(50, 1, 10, 2, grassPlat);
	platformObjects.push_back(starir1_1);

	Platforms* starir1_2 = new Platforms(52, 3, 6, 2, grassPlat);
	platformObjects.push_back(starir1_2);

	Platforms* starir1_3 = new Platforms(54, 5, 2, 2, grassPlat);
	platformObjects.push_back(starir1_3);

	Traps* spikes2 = new Traps(74, -7, 8, 2);
	spikes2->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes2);

	Enemies* zom1 = new Enemies(57, 1, 2, 2, 69);
	zom1->setTextures();
	enemies.push_back(zom1);

	Platforms* plat1 = new Platforms(66, 10, 8, 2, grassPlat);
	platformObjects.push_back(plat1);

	Platforms* plat2 = new Platforms(76, 16, 8, 2, grassPlat);
	platformObjects.push_back(plat2);

	Platforms* plat3 = new Platforms(88, 6, 8, 2, grassPlat);
	platformObjects.push_back(plat3);

	Enemies* zom2 = new Enemies(85, 7, 2, 2, 91);
	zom2->setTextures();
	enemies.push_back(zom2);

	Platforms* floor3 = new Platforms(93, -1, 30, 2, grassPlat);
	platformObjects.push_back(floor3);

	Enemies* zom3 = new Enemies(79, 1, 2, 2, 107);
	zom3->setTextures();
	enemies.push_back(zom3);

	Traps* spikes3 = new Traps(115, -7, 14, 2);
	spikes3->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes3);

	Platforms* plat4 = new Platforms(116, 5, 8, 2, grassPlat);
	platformObjects.push_back(plat4);

	Platforms* floor4 = new Platforms(130, -1, 16, 2, grassPlat);
	platformObjects.push_back(floor4);

	Platforms* plat5 = new Platforms(132, 10, 8, 2, grassPlat);
	platformObjects.push_back(plat5);

	Platforms* plat6 = new Platforms(148, 10, 8, 2, grassPlat);
	platformObjects.push_back(plat6);

	Enemies* zom4 = new Enemies(145, 11, 2, 2, 151);
	zom4->setTextures();
	enemies.push_back(zom4);

	Platforms* plat7 = new Platforms(143, 16, 8, 2,grassPlat);
	platformObjects.push_back(plat7);

	if (followers.size() < 1){
		Follower* follow1 = new Follower(143, 18, 2, 2, 1);
		follow1->setTextures();
		followers.push_back(follow1);
	}

	Traps* spikes4 = new Traps(154, -7, 32, 2);
	spikes4->setTextures("Trap_textures/spike.png");
	traps.push_back(spikes4);

	Platforms* plat8 = new Platforms(143, 5, 4, 2, grassPlat);
	platformObjects.push_back(plat8);

	Platforms* plat9 = new Platforms(155, 5, 4, 2, grassPlat);
	platformObjects.push_back(plat9);

	Platforms* floor5 = new Platforms(164, -1, 8, 2, grassPlat);
	platformObjects.push_back(floor5);

	Platforms* rightWall = new Platforms(181, 0, 2, 300,brickBase);
	platformObjects.push_back(rightWall);

	//********************************************************
	//Floor 2
	//********************************************************
	Platforms* base2 = new Platforms(100, -57, 160, 2, brickBase);
	platformObjects.push_back(base2);

	Platforms* floor6 = new Platforms(155, -50, 50, 2, brickPlat);
	platformObjects.push_back(floor6);

	Torch* torch8 = new Torch(155, -40);
	torches.push_back(torch8);

	Enemies* zom5 = new Enemies(158, -48, 2, 2, 178);
	zom5->setTextures();
	enemies.push_back(zom5);

	Platforms* stair2_1 = new Platforms(155, -48, 4, 2, brickPlat);
	platformObjects.push_back(stair2_1);

	Saw* saw1 = new Saw(131, -46, 2.5, 150, 0);
	saw1->setTextures();
	saws.push_back(saw1);

	Torch* torch9 = new Torch(115, -40);
	torches.push_back(torch9);

	Platforms* plat10 = new Platforms(145, -42, 6, 2, brickPlat);
	platformObjects.push_back(plat10);

	Platforms* plat11 = new Platforms(134, -36, 8, 2, brickPlat);
	platformObjects.push_back(plat11);

	Platforms* plat12 = new Platforms(122, -30, 10, 2, brickPlat);
	platformObjects.push_back(plat12);


	Enemies* zom6 = new Enemies(131, -34, 2, 2, 137);
	zom6->setTextures();
	enemies.push_back(zom6);

	Traps* lava1 = new Traps(102, -53, 158, 6);
	lava1->setTextures("Trap_textures/lava.png");
	traps.push_back(lava1);

	MovingPlatforms* mPlat1 = new MovingPlatforms(110, -49, 4, 2, 128, 0);
	mPlat1->setTextures();
	mPlatformObjects.push_back(mPlat1);

	MovingPlatforms* mPlat2 = new MovingPlatforms(95, -44, 4, 2, 110, 0);
	mPlat2->setTextures();
	mPlatformObjects.push_back(mPlat2);

	Torch* torch10 = new Torch(85, -35);
	torches.push_back(torch10);

	Platforms* floor7 = new Platforms(72.5, -50, 35, 2, brickPlat);
	platformObjects.push_back(floor7);

	Enemies* zom7 = new Enemies(75, -48, 2, 2, 89);
	zom7->setTextures();
	enemies.push_back(zom7);

	Platforms* plat13 = new Platforms(75, -44, 10, 2, brickPlat);
	platformObjects.push_back(plat13);

	Torch* torch11 = new Torch(60, -35);
	torches.push_back(torch11);

	Saw* saw2 = new Saw(54, -46, 2.5, 67, 0);
	saw2->setTextures();
	saws.push_back(saw2);


	Platforms* plat14 = new Platforms(60, -39, 10, 2, brickPlat);
	platformObjects.push_back(plat14);

	if (followers.size() < 2){
		Follower* follow2 = new Follower(45, -32, 2, 2, 2);
		follow2->setTextures();
		followers.push_back(follow2);
	}

	Platforms* plat15 = new Platforms(45, -34, 8, 2, brickPlat);
	platformObjects.push_back(plat15);

	Torch* torch12 = new Torch(35, -40);
	torches.push_back(torch12);

	Platforms* floor8 = new Platforms(27.5, -50, 15, 2, brickPlat);
	platformObjects.push_back(floor8);

	Enemies* zom8 = new Enemies(21, -48, 2, 2, 34);
	zom8->setTextures();
	enemies.push_back(zom8);

	Platforms* filler = new Platforms(21.5, -53, 3, 6, brickBase);
	platformObjects.push_back(filler);

	Torch* torch13 = new Torch(15, -40);
	torches.push_back(torch13);

	Platforms* leftWall = new Platforms(-1, 0, 2, 300, brickBase);
	platformObjects.push_back(leftWall);

	//********************************************************
	//Floor 3
	//********************************************************
	Platforms* base3 = new Platforms(90, -87, 180, 2, brickBase);
	platformObjects.push_back(base3);

	Platforms* plat16 = new Platforms(20, -72, 8, 2, brickPlat);
	platformObjects.push_back(plat16);

	Enemies* zom9 = new Enemies(17, -71, 2, 2, 22);
	zom9->setTextures();
	enemies.push_back(zom9);

	Platforms* floor9 = new Platforms(30, -80, 20, 2, brickPlat);
	platformObjects.push_back(floor9);

	Torch* torch1 = new Torch(30, -72);
	torches.push_back(torch1);

	Enemies* zom10 = new Enemies(21, -79, 2, 2, 40);
	zom10->setTextures();
	enemies.push_back(zom10);

	Traps* lava2 = new Traps(90, -83, 180, 6);
	lava2->setTextures("Trap_textures/lava.png");
	traps.push_back(lava2);

	MovingPlatforms* mPlat3 = new MovingPlatforms(45, -75, 4, 2, 60, 0);
	mPlat3->setTextures();
	mPlatformObjects.push_back(mPlat3);

	Torch* torch2 = new Torch(48, -72);
	torches.push_back(torch2);

	MovingPlatforms* mPlat4 = new MovingPlatforms(65, -80, 4, 2, 0, -60);
	mPlat4->setTextures();
	mPlatformObjects.push_back(mPlat4);

	Torch* torch3 = new Torch(70, -72);
	torches.push_back(torch3);

	MovingPlatforms* mPlat5 = new MovingPlatforms(70, -75, 4, 2, 100, 0);
	mPlat5->setTextures();
	mPlatformObjects.push_back(mPlat5);

	Saw* saw3 = new Saw(84, -80, 2.5, 0, -60);
	saw3->setTextures();
	saws.push_back(saw3);

	MovingPlatforms* mPlat6 = new MovingPlatforms(75, -68, 4, 2, 100, 0);
	mPlat6->setTextures();
	mPlatformObjects.push_back(mPlat6);

	Torch* torch4 = new Torch(98, -72);
	torches.push_back(torch4);

	MovingPlatforms* mPlat7 = new MovingPlatforms(110, -80, 4, 2, 0, -60);
	mPlat7->setTextures();
	mPlatformObjects.push_back(mPlat7);

	MovingPlatforms* mPlat8 = new MovingPlatforms(120, -68, 4, 2, 135, 0);
	mPlat8->setTextures();
	mPlatformObjects.push_back(mPlat8);

	Torch* torch5 = new Torch(125, -72);
	torches.push_back(torch5);

	MovingPlatforms* mPlat9 = new MovingPlatforms(130, -75, 4, 2, 150, 0);
	mPlat9->setTextures();
	mPlatformObjects.push_back(mPlat9);

	Saw* saw4 = new Saw(152, -80, 2.5, 0, -60);
	saw4->setTextures();
	saws.push_back(saw4);

	Torch* torch6 = new Torch(160, -72);
	torches.push_back(torch6);

	Platforms* floor10 = new Platforms(167.5, -80, 25, 2, brickPlat);
	platformObjects.push_back(floor10);

	exit = new Exit(170, -74, 8, 10);
	exit->setTextures();

	Torch* torch7 = new Torch(176, -72);
	torches.push_back(torch7);

	player = new Player(2,2,2,2);
	camera =  new Camera(2, 2);
	if (followers.back()->isActive()){
		player->setPos(45,-32);
		camera->setPos(45,-32);
	}
	else if (followers.front()->isActive()){
		player->setPos(143,18);
		camera->setPos(143,18);
	}
	//player = new Player(160, -75, 2, 2);
	//camera = new Camera(160, 175);
	player->setTextures();

}

void LevelOne::onSwitchOut(){
	traps.clear();
	platformObjects.clear();
	enemies.clear();
	mPlatformObjects.clear();
	saws.clear();
	torches.clear();
}

void LevelOne::finish(){
	traps.clear();
	platformObjects.clear();
	enemies.clear();
	mPlatformObjects.clear();
	saws.clear();
	torches.clear();
	followers.clear();

	camera = NULL;
	player = NULL;
	exit = NULL;
	hud = NULL;
}

void LevelOne::onReshape(int w, int h)
{

	glViewport(0,0,w,h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();;

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	//gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect,  -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glOrtho(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect,  -VIEW_SIZE*0.5, VIEW_SIZE*0.5, -VIEW_SIZE-1, VIEW_SIZE);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

void LevelOne::update(double deltaT, double prevDeltaT)
{
	//cout << deltaT << endl;
	if (!paused){
		glClear(GL_COLOR_BUFFER_BIT);

		//Player checks
		player->setCollidables();
		player->update(deltaT, prevDeltaT);
		for each(Follower* f in followers){
			f->setCollidables();
			f->update(deltaT, prevDeltaT, player);
			f->checkForCollisions(player, deltaT, prevDeltaT);
		}
		for each(Platforms* s in platformObjects){
			s->setCollidables();
			s->update(deltaT, prevDeltaT);
			player->checkForCollisions(s, deltaT, prevDeltaT);
		}
		for each(MovingPlatforms* mp in mPlatformObjects){
			mp->setCollidables();
			mp->update(deltaT,prevDeltaT);
			player->checkForCollisions(mp, deltaT, prevDeltaT);
		}
		for each(Traps* d in traps){
			d->setCollidables();
			d->update(deltaT, prevDeltaT);
			player->checkForCollisions(d, deltaT, prevDeltaT);
		}
		for each(Saw* s in saws){
			s->setCollidables();
			s->update(deltaT, prevDeltaT);
			s->checkForCollisions(player,deltaT,prevDeltaT);
		}

		//enemy checks
		for each(Enemies* e in enemies){
			if(!e->checkDeath()){
				e->setCollidables();
				bool chase = e->checkIfChase(player);
				if (chase){
					e->chase(player, deltaT, prevDeltaT);
				}
			e->update(deltaT, prevDeltaT);
			e->checkForCollisions(player, deltaT, prevDeltaT);
			}

			for each(Traps* d in traps){
				e->checkForCollisions(d, deltaT, prevDeltaT);
			}
			for each(Platforms* s in platformObjects){
				e->checkForCollisions(s, deltaT, prevDeltaT);
			}
		}

		for each(Torch* t in torches){
			t->update(deltaT, prevDeltaT);
		}
		for each(Gem* g in gems){
			if (!g->checkCollected()){
				g->update(deltaT, prevDeltaT);
				if (g->checkForCollisions(player, deltaT, prevDeltaT)){
					hud->gemCollected();
				}
			}
		}

		camera->updatePos(deltaT, prevDeltaT,player);	
		exit->setCollidables();
		exit->update(deltaT, prevDeltaT);
		hud->setCollidables();
		hud->update(deltaT, prevDeltaT);
		hud->updatePos(camera);
		}

		if (exit->checkForCollisions(player, deltaT, prevDeltaT))
			app->finishLevel(hud, 1);
		else{
			if (!hud->checkTime()){
  				app->timeRanOut();
			}
		}
		
			
	glFlush;
}



void LevelOne::draw()
{
	
	if (!paused){
		glClear(GL_COLOR_BUFFER_BIT);

		glLoadIdentity();
		gluLookAt(camera->getPos().getX(), camera->getPos().getY(), 1.0f, camera->getPos().getX(), camera->getPos().getY(), 0.0f, 0, 1, 0);
		//cout << "cam X : " << camera->getPos().getX() << endl;
		//cout << "cam Y : " << camera->getPos().getY() << endl;
		drawBackground();

		exit->draw();
		for each(Traps* t in traps){
			t->draw();
		}
		for each(Enemies* e in enemies){
			e->draw();
		}
		for each(MovingPlatforms* mp in mPlatformObjects){
			mp->draw();
		}
		for each(Saw* s in saws){
			s->draw();
		}
		for each(Platforms* s in platformObjects){
			s->draw();
		}
		for each(Torch* t in torches){
			t->draw();
		}
		for each(Gem* g in gems){
			g->draw();
		}
		for each(Follower* f in followers){
			f->draw();
		}
		
		hud->draw();

		//player is alive
		if (!player->checkLife())
			player->draw();
		//player is dead
		else{
			hud->incrementCounter();
			app->playerDied();
		}
	}

	glFlush;
}

void LevelOne::drawBackground(){
	double aspect = app->getAspectRatio();

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background4);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-25, 49);
			glTexCoord2f(0, 1); glVertex2f(-25, 24);
			glTexCoord2f(250/25, 1); glVertex2f(225, 24);
			glTexCoord2f(250/25, 0); glVertex2f(225, 49);
		glEnd();
		//pop the matrix
	glPopMatrix();
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background1);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-25, 0);
			glTexCoord2f(0, 1); glVertex2f(-25, 25);
			glTexCoord2f(250/25, 1); glVertex2f(225, 25);
			glTexCoord2f(250/25, 0); glVertex2f(225, 0);
		glEnd();
		//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);

	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background2);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-25, -10);
			glTexCoord2f(0, 1); glVertex2f(-25, 0);
			glTexCoord2f(225 / 10, 1); glVertex2f(225, 0);
			glTexCoord2f(225 / 10, 0); glVertex2f(225, -10);
		glEnd();
		//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	
	
	glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, background3);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
				glTexCoord2f(0.0, 0.0);glVertex2f(-25,-10);
				glTexCoord2f(0.0, 50);glVertex2f(-25,-100);
				glTexCoord2f(112.5,50);glVertex2f(225,-100);
				glTexCoord2f(112.5, 0.0);glVertex2f(225,-10);
		glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}



void LevelOne::onMouseDown(int button, float mouseX, float mouseY){}		// Called when mouse button pressed
void LevelOne::onMouseUp(int button, float mouseX, float mouseY){}		// Called when mouse button released
void LevelOne::onMouseMove(float mouseX, float mouseY){}					// Called when mouse moved

void LevelOne::onKeyDown(int key){									// Called when key pressed

	if (key == 0x25 || key == 65){
		player->setVelX(-100.0f);
		player->setDirection(-1);

	}
	if (key == 0x27 || key == 68){
		player->setVelX(100.0f);
		player->setDirection(1);

	}
	if (key == 0x26 || key == ' '){
		if (!player->getJump()){
			player->setVelY(250.0f);
			player->setJump(true);
		}
	}

}									
void LevelOne::onKeyUp(int key){									// Called when key released

	if (key == 80){
		paused = !paused;
	}


	//if the velocity of x is greater than 0 slowly decrease it
	if (key == 0x25 || key == 65){//&& movingObjects.front()->getVel().getX() < 0){
		player->setVelX(0.0f);
	}
	//else slowly increase it
	if (key == 0x27 || key == 68){// && movingObjects.front()->getVel().getX() > 0){
		player->setVelX(0.0f);
	}


}										
