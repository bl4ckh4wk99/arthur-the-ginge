#pragma once

#include "CollidableObjects.h"
#include <Windows.h>
#include "glew.h"
#include <gl\gl.h>			// Header File For The OpenGL32 Library
#include <gl\glu.h>			// Header File For The GLu32 Library

class BoundingCircle : public CollidableObjects{
public:

	//constructors
	BoundingCircle();
	BoundingCircle(Vec2 pos, float radius);

	//equality operator
	BoundingCircle operator = (const BoundingCircle &v);
	//draw method
	void draw();
	//update method
	void update(Vec2 attachedNewPos);
};