#include "Exit.h"

Exit::Exit(Vec2 pos, Vec2 size){
	this->pos = pos;
	this->size = size;
	frames = 0;
	activated = false;
}

Exit::Exit(float x, float y, float width, float height){
	Vec2 pos = Vec2(x,y);
	Vec2 size = Vec2(width,height);
	this->pos = pos;
	this->size = size;
	frames = 0;
	activated = false;
}

void Exit::draw(){
	//push the matrix
		glPushMatrix();
		//start textures
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glBindTexture(GL_TEXTURE_2D, currentTexture);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//draw the object using its size and positions variables
		glBegin(GL_POLYGON);
				glTexCoord2f(0.0, 0.0);glVertex2f(this->pos.getX() - this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
				glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
				glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
				glTexCoord2f(1.0, 0.0);glVertex2f(this->pos.getX() + this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
		glEnd();
		//pop the matrix
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);

		circle.draw();
		box.draw();
}
void Exit::update(double deltaT, double prevDeltaT){
	if(activated){
		frames += deltaT;
		int it = frames;
		if(it > 4)
			it = 4;
		currentTexture = idle_textures.at(it);
	}
	else{
		currentTexture = idle_textures.front();
	}
	//get its x and y pos
	pos.getX();
	pos.getY();
	//update its collidables
	box.update(this->pos);
	circle.update(this->pos);
}
bool Exit::checkForCollisions(Player* p, double deltaT, double prevDeltaT){
	//bool variable to store results
	bool collision = false;
	//bool variable for if the circles are colliding
	bool circleTouch = false;
	//get the 2 bounding circles positions and radius
	float x1 = this->circle.getPos().getX();
	float x2 = p->getCircle().getPos().getX();
	float y1 = this->circle.getPos().getY();
	float y2 = p->getCircle().getPos().getY();
	float r1 = this->circle.getSize().getX();
	float r2 = p->getCircle().getSize().getX();

	//Check circles first
	if((x1 - x2)*(x1-x2) + (y1-y2)*(y1-y2) < (r1+r2)*(r1+r2)){
		circleTouch = true;
	}

	//if circles collided do more thorough checking
	if(circleTouch){
		//get the 2 bounding boxes verticies 
		float xMinA = (this->box.getPos().getX()) - this->box.getSize().getX()/2;
		float xMaxB = p->getBox().getPos().getX() + p->getBox().getSize().getX()/2;
		float xMaxA = (this->box.getPos().getX()) + this->box.getSize().getX()/2;
		float xMinB = p->getBox().getPos().getX() - p->getBox().getSize().getX()/2;
		float yMinA = (this->box.getPos().getY()) - this->box.getSize().getY()/2;
		float yMaxB = p->getBox().getPos().getY() + p->getBox().getSize().getY()/2;
		float yMaxA = (this->box.getPos().getY()) + this->box.getSize().getY()/2;
		float yMinB = p->getBox().getPos().getY() - p->getBox().getSize().getY()/2;

		//bool variables for each side
		bool checkLeft = false;
		bool checkRight = false;
		bool checkBottom = false;
		bool checkTop = false;

		//check which side is being collided
		if(xMinA <= xMaxB && xMinA >= xMinB)
			checkLeft = true;
			if(xMaxA >= xMinB && xMaxA <= xMaxB)
				checkRight = true;
			if(yMinA <= yMaxB && yMinA >= yMinB)
				checkBottom = true;
			if(yMaxA >= yMinB && yMaxA <= yMaxB)
				checkTop = true;
			// check left and if true respond accordingly
			if(checkLeft && checkBottom && checkTop || checkLeft && checkBottom){ 
				activated = true;
				return true;
			}
			// check right and if true respond accordingly
			if(checkRight && checkBottom && checkTop || checkRight && checkBottom|| checkRight && checkTop){  
				activated = true;
				return true;
			}
			//check bottom and if true respond accordingly
			if(checkBottom && checkRight|| checkBottom && checkLeft	|| checkBottom && checkRight && checkLeft){  
				activated = true;
				return true;
			}
			//check top and if true respond accordingly
			if(checkTop && checkRight|| checkTop && checkLeft ||checkTop && checkRight && checkLeft){ 
				activated = true;
				return true;
			}
		}
		
		//return the result boolean
		return collision;
}

void Exit::setTextures(){
	GLuint idle1 = loadPNG("World_textures/door1.png");   
	GLuint idle2 = loadPNG("World_textures/door2.png");   
	GLuint idle3 = loadPNG("World_textures/door3.png");   
	GLuint idle4 = loadPNG("World_textures/door4.png");   
	GLuint idle5 = loadPNG("World_textures/door5.png");   


	this->addIdleTexture(idle1);
	this->addIdleTexture(idle2);
	this->addIdleTexture(idle3);
	this->addIdleTexture(idle4);
	this->addIdleTexture(idle5);

}