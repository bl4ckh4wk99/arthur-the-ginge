//
// This is taken from the Graphics 1 OpenGL template
//

#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls
#include "EndActivity.h"
#include "LoadPNG.h"



EndActivity::EndActivity(OpenGLApplication *app)
	: Activity(app)
{
}


void EndActivity::initialise()
{
	onSwitchIn();

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


void EndActivity::shutdown()
{
	glDeleteTextures(1, &textureID);
}


void EndActivity::onSwitchOut()
{
	shutdown();
}


void EndActivity::onSwitchIn()
{
	// Activity switched in

	glClearColor(1.0,0.0,0.0,0.0);						//sets the clear colour to black

	// Initialise the activity; called at application start up
	LoadPNG imageloader = LoadPNG();
	// Load the start screen image as a texture using the SOIL library
	textureID = imageloader.loadPNG("Background_textures/Death_Skull.png");
}

void EndActivity::finish()
{
	shutdown();
}

void EndActivity::onReshape(int width, int height)
{
	// Screen resized
	glViewport(0,0,width,height);						// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();
	gluOrtho2D(-aspect, aspect, -1.0, 1.0);				// set the coordinate system for the window

	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();									// Reset The Modelview Matrix
}

void EndActivity::draw()
{
	glPushMatrix();
		// OpenGL render calls go in this method
		double aspect = app->getAspectRatio();
		// Clear color buffer
		glClear(GL_COLOR_BUFFER_BIT);
	
		// Identity matrix
		glLoadIdentity();

		//myfont.Begin();
		//myfont.DrawString("The Boy Who Lived", 0.5, 0.5, 440.0f);

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, textureID);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0);glVertex2f(-aspect, -1);
			glTexCoord2f(0, 1);glVertex2f(-aspect, 1);
			glTexCoord2f(1, 1);glVertex2f(aspect, 1);
			glTexCoord2f(1, 0);glVertex2f(aspect, -1);
		glEnd();

		//pop the matrix
		glPopMatrix();

	// Disable 2D texturing
	glDisable(GL_TEXTURE_2D);

	glFlush();
}


void EndActivity::onMouseDown(int button, float mouseX, float mouseY){		// Called when mouse button pressed

}
void EndActivity::onMouseUp(int button, float mouseX, float mouseY){			// Called when mouse button released

}
void EndActivity::onMouseMove(float mouseX, float mouseY){					// Called when mouse moved

}

void EndActivity::onKeyDown(int key){										// Called when key released

}	

void EndActivity::onKeyUp(int key){										// Called when key released
	if (key == ' ')
	{
		// Space
		app->setCurrentActivity(app->game);
	}

}		

