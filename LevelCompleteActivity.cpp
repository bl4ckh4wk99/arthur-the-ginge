#include "LevelCompleteActivity.h"

LevelCompleteActivity::LevelCompleteActivity(OpenGLApplication *app)
	:Activity(app)
{}

void LevelCompleteActivity::initialise(){										// Called on application start up

}
void LevelCompleteActivity::shutdown(){											// Called on application shut down

}
void LevelCompleteActivity::finish(){												// Called when the activity is finished and needs to be rebuilt from scratch

}

void LevelCompleteActivity::onSwitchIn(){											// Activity switch in; called when the activity changes and this one switches in

}
void LevelCompleteActivity::onSwitchOut(){


}
void LevelCompleteActivity::onReshape(int width, int height){						// called when the window is resized

}
void LevelCompleteActivity::update(double deltaT, double prevDeltaT){				// Update the application;

}
void LevelCompleteActivity::draw(){												// Render function


}
void LevelCompleteActivity::drawBackground(){										//Render background textures

}


// Event handler methods; these methods are called in response to user actions:
void LevelCompleteActivity::onKeyDown(int key){									// Called when key pressed

}
void LevelCompleteActivity::onKeyUp(int key){									// Called when key released

}
void LevelCompleteActivity::onMouseDown(int button, float mouseX, float mouseY){		// Called when mouse button pressed

}
void LevelCompleteActivity::onMouseUp(int button, float mouseX, float mouseY){			// Called when mouse button released


}
void LevelCompleteActivity::onMouseMove(float mouseX, float mouseY){					// Called when mouse moved

}