#include "LoadPNG.h"


LoadPNG::LoadPNG(){}

GLuint LoadPNG::loadPNG(const char * filename){
	GLuint retVal;
		//following to be placed in the init function
		nv::Image img;
		// Return true on success
		if (img.loadImageFromFile(filename))
		{
			glGenTextures(1, &retVal);
			glBindTexture(GL_TEXTURE_2D, retVal);
			glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
			glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
			glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(),
			img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
			GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		}
		else
			MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);


		return retVal;
}
