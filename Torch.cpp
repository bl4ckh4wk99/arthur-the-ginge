#include "Torch.h"

Torch::Torch(Vec2 pos){
	this->pos = pos;
	this->size = Vec2(5,4);
	frames = 0;
	setTextures();
}

Torch::Torch(float x, float y){
	Vec2 pos = Vec2(x,y);
	this->pos = pos;
	this->size = Vec2(5,4);
	frames = 0;
	setTextures();
}


//draw method
void Torch::draw(){
	//push the matrix
	glPushMatrix();
	//start textures
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, currentTexture);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//draw the object using its size and positions variables
	glBegin(GL_POLYGON);
			glTexCoord2f(0.0, 0.0);glVertex2f(this->pos.getX() - this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
			glTexCoord2f(0.0, 1.0); glVertex2f(this->pos.getX() - this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(1.0, 1.0); glVertex2f(this->pos.getX() + this->size.getX() / 2, this->pos.getY() + this->size.getY() / 2);
			glTexCoord2f(1.0, 0.0);glVertex2f(this->pos.getX() + this->size.getX()/2, this->pos.getY() - this->size.getY()/2);
	glEnd();
	//pop the matrix
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}
//update method
void Torch::update(double deltaT, double prevDeltaT){

	frames += deltaT * 5;

	int it = (int)frames;
	if(it > 1){
		frames = 0;
		it = 0;
	}
	currentTexture = idle_textures.at(it);

}
	
//set collidables for this object
void Torch::setCollidables(){}


void Torch::setTextures(){
	GLuint idle1 = loadPNG("World_textures/torch-1.png");                                      
	this->addIdleTexture(idle1);
	GLuint idle2 = loadPNG("World_textures/torch-2.png");                                      
	this->addIdleTexture(idle2);
}
