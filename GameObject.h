#pragma once
#include "Vec2.h"
#include "BoundingCircle.h"
#include "BoundingBox.h"
#include "nvImage.h"


#include <vector>
#include <iostream>
using namespace std;

class GameObject{
protected:
	Vec2 pos, size;
	BoundingCircle circle;
	BoundingBox box;
	GLuint currentTexture;
	vector<GLuint> idle_textures;

public:
	//constructors
	GameObject();
	GameObject(Vec2 ipos, Vec2 isize);
	GameObject(float x, float y, float width, float height);
	//getters and setters
	Vec2 getPos();
	void setPos(float x, float y);

	Vec2 getSize();
	void setSize(float width, float height);

	BoundingCircle getCircle();
	void setCircle(BoundingCircle circle);

	BoundingBox getBox();
	void setBox(BoundingBox box);

	vector<GLuint> getIdleTextures();
	void addIdleTexture(GLuint texture);

	//abstract methods for drawing and update
	virtual void draw() = 0;
	virtual void update(double deltaT, double prevDeltaT)= 0;
	//abstract methods for setting collidables and textures
	virtual void setCollidables() = 0;

	virtual void setTextures() = 0;
	//load PNG method
	GLuint loadPNG(const char * filename);
	
};