#pragma once

#include "GameObject.h"
#include "Player.h"

class Camera : public GameObject{
private:

public:
	//constructors
	Camera(Vec2 ipos);

	Camera(float x, float y);
	//draw method
	void draw();
	//update methods
	void updatePos(double deltaT, double prevDeltaT, Player* p);
	void update(double deltaT, double prevDeltaT);
	//set collidables
	void setCollidables();
	//set textures
	void setTextures();

};