#include "LevelSelect.h"

#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE

LevelSelect::LevelSelect(OpenGLApplication *app)
	:Activity(app)
{}

void LevelSelect::initialise(){											// Called on application start up

	
	LoadPNG imageloader = LoadPNG();
	backgroundTex = imageloader.loadPNG("Background_textures/Full-background.png");

	GLuint temp = imageloader.loadPNG("Background_textures/zero.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/one.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/two.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/three.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/four.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/five.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/six.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/seven.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/eight.png");
	numbers.push_back(temp);
	temp = imageloader.loadPNG("Background_textures/nine.png");
	numbers.push_back(temp);

	
	highscoreTex = imageloader.loadPNG("Background_textures/Highscore.png");

}
void LevelSelect::shutdown(){											// Called on application shut down
	buttons.clear();
}
void LevelSelect::finish(){												// Called when the activity is finished and needs to be rebuilt from scratch
	shutdown();
}

void LevelSelect::onSwitchIn(){											// Activity switch in; called when the activity changes and this one switches in
	Button* menu = new Button(0, -10, 20, 5, "Background_textures/Menu.png", "Background_textures/Menu-select.png", 1);
	buttons.push_back(menu);

	Button* levelOne = new Button(-10, 7.5, 15, 15, "Background_textures/levelOne.png", "Background_textures/levelOne-select.png",2);
	buttons.push_back(levelOne);

	Button* levelTwo = new Button(10, 7.5, 15, 15, "Background_textures/levelTwo.png", "Background_textures/levelTwo-select.png",3);
	buttons.push_back(levelTwo);

	ifstream firstScore("highscore1.txt");
	//get the line
	string score1Line;
	//iterate over the lines in the file
	while (getline(firstScore, score1Line)){
		string temp;

		//get the highscore
		temp = score1Line.substr(0, score1Line.find('\n'));
		//convert it to int
		stringstream(temp) >> highscore1;
	}
	
	firstScore.close();

	ifstream secondScore("highscore2.txt");
	//get the line
	string score2Line;
	//iterate over the lines in the file
	while (getline(secondScore, score2Line)){
		string temp;

		//get the highscore
		temp = score2Line.substr(0, score2Line.find('\n'));
		//convert it to int
		stringstream(temp) >> highscore2;
	}
	
	secondScore.close();
}
void LevelSelect::onReshape(int w, int h){						// called when the window is resized
	glViewport(0, 0, w, h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();;

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect, -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();
}
void LevelSelect::update(double deltaT, double prevDeltaT){				// Update the application;
	for each(Button* b in buttons){
		b->setCollidables();
		b->update(deltaT, prevDeltaT);
	}
}
void LevelSelect::draw(){
	// OpenGL render calls go in this method
	double aspect = app->getAspectRatio();
	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, backgroundTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	for each(Button* b in buttons){
		b->draw();
	}

	drawHighscores();

	glFlush();

}

void LevelSelect::drawHighscores(){
	int tempScore = highscore1;

	int tensTho1 = tempScore / 10000;
	if (tensTho1 < 0)
		tensTho1 = 0;
	tempScore = tempScore % 10000;
	int tho1 = tempScore / 1000;
	if (tho1 < 0)
		tho1 = 0;
	tempScore = tempScore % 1000;
	int hun1 = tempScore / 100;
	if (hun1 < 0)
		hun1 = 0;
	tempScore = tempScore % 100;
	int tens1 = tempScore / 10;
	if (tens1 < 0)
		tens1 = 0;
	tempScore = tempScore % 10;
	int units1 = tempScore;
	if (units1 < 0)
		units1 = 0;


	tempScore = highscore2;

	int tensTho2 = tempScore / 10000;
	if (tensTho2 < 0)
		tensTho2 = 0;
	tempScore = tempScore % 10000;
	int tho2 = tempScore / 1000;
	if (tho2 < 0)
		tho2 = 0;
	tempScore = tempScore % 1000;
	int hun2 = tempScore / 100;
	if (hun2 < 0)
		hun2 = 0;
	tempScore = tempScore % 100;
	int tens2 = tempScore / 10;
	if (tens2 < 0)
		tens2 = 0;
	tempScore = tempScore % 10;
	int units2 = tempScore;
	if (units2 < 0)
		units2 = 0;



	//************************************************************************************************
	//************************************LEVEL ONE HIGHSCORE*****************************************
	//************************************************************************************************

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, highscoreTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-15, -2);
			glTexCoord2f(0, 1); glVertex2f(-15, 0);
			glTexCoord2f(1, 1); glVertex2f(-5, 0);
			glTexCoord2f(1, 0); glVertex2f(-5, -2);
		glEnd();


	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tensTho1));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-15, -4);
			glTexCoord2f(0, 1); glVertex2f(-15, -2);
			glTexCoord2f(1, 1); glVertex2f(-13, -2);
			glTexCoord2f(1, 0); glVertex2f(-13, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tho1));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-13, -4);
			glTexCoord2f(0, 1); glVertex2f(-13, -2);
			glTexCoord2f(1, 1); glVertex2f(-11, -2);
			glTexCoord2f(1, 0); glVertex2f(-11, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(hun1));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-11, -4);
			glTexCoord2f(0, 1); glVertex2f(-11, -2);
			glTexCoord2f(1, 1); glVertex2f(-9, -2);
			glTexCoord2f(1, 0); glVertex2f(-9, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tens1));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-9, -4);
			glTexCoord2f(0, 1); glVertex2f(-9, -2);
			glTexCoord2f(1, 1); glVertex2f(-7, -2);
			glTexCoord2f(1, 0); glVertex2f(-7, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(units1));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-7, -4);
			glTexCoord2f(0, 1); glVertex2f(-7, -2);
			glTexCoord2f(1, 1); glVertex2f(-5, -2);
			glTexCoord2f(1, 0); glVertex2f(-5, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	//**********************************************************************************************
	//***************************************LEVEL 2 HIGH SCORE*************************************
	//**********************************************************************************************


	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, highscoreTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(5, -2);
			glTexCoord2f(0, 1); glVertex2f(5, 0);
			glTexCoord2f(1, 1); glVertex2f(15, 0);
			glTexCoord2f(1, 0); glVertex2f(15, -2);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();
		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(units2));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(13, -4);
			glTexCoord2f(0, 1); glVertex2f(13, -2);
			glTexCoord2f(1, 1); glVertex2f(15, -2);
			glTexCoord2f(1, 0); glVertex2f(15, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tens2));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(11, -4);
			glTexCoord2f(0, 1); glVertex2f(11, -2);
			glTexCoord2f(1, 1); glVertex2f(13, -2);
			glTexCoord2f(1, 0); glVertex2f(13, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(hun2));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(9, -4);
			glTexCoord2f(0, 1); glVertex2f(9, -2);
			glTexCoord2f(1, 1); glVertex2f(11, -2);
			glTexCoord2f(1, 0); glVertex2f(11, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tho2));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(7, -4);
			glTexCoord2f(0, 1); glVertex2f(7, -2);
			glTexCoord2f(1, 1); glVertex2f(9, -2);
			glTexCoord2f(1, 0); glVertex2f(9, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, numbers.at(tensTho2));
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(5, -4);
			glTexCoord2f(0, 1); glVertex2f(5, -2);
			glTexCoord2f(1, 1); glVertex2f(7, -2);
			glTexCoord2f(1, 0); glVertex2f(7, -4);
		glEnd();

	//pop the matrix
	glPopMatrix();

		
}


// Event handler methods; these methods are called in response to user actions:
void LevelSelect::onKeyDown(int key){									// Called when key pressed

}
void LevelSelect::onKeyUp(int key){									// Called when key released

}
void LevelSelect::onMouseDown(int button, float mouseX, float mouseY){		// Called when mouse button pressed

}
void LevelSelect::onMouseUp(int button, float mouseX, float mouseY){			// Called when mouse button released
	for (int i = 0; i < buttons.size(); i++){
		Button* b = buttons.at(i);
		if (b->isSelected() && b->getID() == 1 && button == 0){
			shutdown();
			app->swicthActivity(app->mainMenu);
		}
		if (b->isSelected() && b->getID() == 2 && button == 0){
			shutdown();
			app->startLevel(app->levelOne);
		}
		if (b->isSelected() && b->getID() == 3 && button == 0){
			shutdown();
			app->startLevel(app->levelTwo);
		}
	}
}
void LevelSelect::onMouseMove(float mouseX, float mouseY){					// Called when mouse moved
	double aspect = app->getAspectRatio();
	double oriAspect = app->getOriAspectRatio();
	float x = ((mouseX - app->getScreenWidth() / 2) / (VIEW_SIZE*0.5*oriAspect*(oriAspect / aspect) * ((double)app->getScreenWidth() / (double)app->getOriScreenWidth())));
	float y = ((app->getScreenHeight() / 2 - mouseY) / (VIEW_SIZE*0.5*oriAspect* ((double)app->getScreenHeight() / (double)app->getOriScreenHeight())));
	for each(Button* b in buttons){
		b->checkForCollisions(x, y);
	}
}