#include "OpenGLApplication.h"			// Needed for OpenGLApplication method calls
#include "ControlActivity.h"
#include "LoadPNG.h"
#include "Button.h"


#define VIEW_SIZE 30.0f				// The height of the view in WORLD SPACE

std::vector<Button*> Buttons;

ControlActivity::ControlActivity(OpenGLApplication *app)
	: Activity(app)		// Call the super constructor
{}

void ControlActivity::initialise()
{
	// Initialise the activity; called at application start up
	LoadPNG imageloader = LoadPNG();
	
	backgroundTex = imageloader.loadPNG("Background_textures/Full-background.png");

	Button* menu = new Button(0, -10, 20, 5, "Background_textures/Menu.png", "Background_textures/Menu-select.png", 1);
	Buttons.push_back(menu);

	leftKey = imageloader.loadPNG("Background_textures/leftKey.png");
	leftKeys.push_back(leftKey);
	GLuint temp = imageloader.loadPNG("Background_textures/leftKey-select.png");
	leftKeys.push_back(temp);
	rightKey = imageloader.loadPNG("Background_textures/rightKey.png");
	rightKeys.push_back(rightKey);
	temp = imageloader.loadPNG("Background_textures/rightKey-select.png");
	rightKeys.push_back(temp);

	aKey = imageloader.loadPNG("Background_textures/aKey.png");
	aKeys.push_back(aKey);
	temp = imageloader.loadPNG("Background_textures/aKey-select.png");
	aKeys.push_back(temp);
	dKey = imageloader.loadPNG("Background_textures/dKey.png");
	dKeys.push_back(dKey);
	temp = imageloader.loadPNG("Background_textures/dKey-select.png");
	dKeys.push_back(temp);

	space = imageloader.loadPNG("Background_textures/space.png");
	spaces.push_back(space);
	temp = imageloader.loadPNG("Background_textures/space-select.png");
	spaces.push_back(temp);

	pKey = imageloader.loadPNG("Background_textures/pKey.png");
	pKeys.push_back(pKey);
	temp = imageloader.loadPNG("Background_textures/pKey-select.png");
	pKeys.push_back(temp);

	leftRight = imageloader.loadPNG("Background_textures/leftRight.png"); 
	pause = imageloader.loadPNG("Background_textures/pause.png");
	jump = imageloader.loadPNG("Background_textures/jump.png");

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

}

void ControlActivity::shutdown()
{
	Buttons.clear();
	glDeleteTextures(1, &backgroundTex);
	glDeleteTextures(1, &menuTex);
	glDeleteTextures(1, &aKey);
	glDeleteTextures(1, &dKey);
	glDeleteTextures(1, &leftKey);
	glDeleteTextures(1, &rightKey);
	glDeleteTextures(1, &space);
	glDeleteTextures(1, &pKey);
	glDeleteTextures(1, &leftRight);
	glDeleteTextures(1, &jump);
	glDeleteTextures(1, &pause);

	menuTexs.clear();
	spaces.clear();
	rightKeys.clear();
	leftKeys.clear();
	dKeys.clear();
	aKeys.clear();
	pKeys.clear();

	
}

void ControlActivity::onSwitchIn()
{

	glClearColor(0.0, 0.0, 0.0, 1.0);						//sets the clear colour to black
}
void ControlActivity::finish(){
	shutdown();
}


void ControlActivity::onReshape(int w, int h)
{
	glViewport(0, 0, w, h);								// Reset The Current Viewport

	glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
	glLoadIdentity();									// Reset The Projection Matrix

	double aspect = app->getAspectRatio();;

	// The height of the visible area is defined by VIEW_SIZE. Split it half each way around the origin, hence the *0.5
	// Take the aspect ratio into consideration when computing the width of the visible area
	gluOrtho2D(-VIEW_SIZE*0.5*aspect, VIEW_SIZE*0.5*aspect, -VIEW_SIZE*0.5, VIEW_SIZE*0.5);
	glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
	glLoadIdentity();
}

void ControlActivity::draw()
{
	// OpenGL render calls go in this method
	double aspect = app->getAspectRatio();
	// Clear color buffer
	glClear(GL_COLOR_BUFFER_BIT);

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, backgroundTex);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 * aspect, VIEW_SIZE*0.5);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 * aspect, -VIEW_SIZE*0.5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, leftKey);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 1.1, 8);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 1.1, 5);
			glTexCoord2f(1, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 1.5, 5);
			glTexCoord2f(1, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 1.5, 8);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, rightKey);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 2, 8);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 2, 5);
			glTexCoord2f(1, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 4, 5);
			glTexCoord2f(1, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 4, 8);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, leftRight);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 2, 8);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 2, 10);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 2, 10);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 2, 8);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, aKey);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
			glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 4, 5);
			glTexCoord2f(0, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 4, 8);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 2, 8);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 2, 5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, dKey);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 1.5, 5);
			glTexCoord2f(0, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 1.5, 8);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 1.1, 8);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 1.1, 5);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, jump);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect, 0);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect, 3);
			glTexCoord2f(1, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 3, 3);
			glTexCoord2f(1, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 3, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, space);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect, -2);
			glTexCoord2f(0, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect, 1);
			glTexCoord2f(1, 1); glVertex2f(-VIEW_SIZE*0.5 *aspect / 3, 1);
			glTexCoord2f(1, 0); glVertex2f(-VIEW_SIZE*0.5 *aspect / 3, -2);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, pause);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 3, 0);
			glTexCoord2f(0, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 3, 3);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 *aspect, 3);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 *aspect, 0);
		glEnd();

	//pop the matrix
	glPopMatrix();

	glPushMatrix();
		// Identity matrix
		glLoadIdentity();

		// Bind our start screen texture to GL_TEXTURE_2D
		glBindTexture(GL_TEXTURE_2D, pKey);
		// Enable 2D texturing
		glEnable(GL_TEXTURE_2D);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//make a square, with texture co-ordinates for each vertex
		glBegin(GL_POLYGON);
			glTexCoord2f(0, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 2.5, -2);
			glTexCoord2f(0, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 2.5, 1);
			glTexCoord2f(1, 1); glVertex2f(VIEW_SIZE*0.5 *aspect / 1.5, 1);
			glTexCoord2f(1, 0); glVertex2f(VIEW_SIZE*0.5 *aspect / 1.5, -2);
		glEnd();

	//pop the matrix
	glPopMatrix();

	for each(Button* b in Buttons){
		b->draw();
	}

	glFlush();
}

void ControlActivity::update(double deltaT, double prevDeltaT){
	for each(Button* b in Buttons){
		b->setCollidables();
		b->update(deltaT, prevDeltaT);
	}
}

void ControlActivity::onMouseDown(int button, float mouseX, float mouseY){	// Called when mouse button pressed

}
void ControlActivity::onMouseUp(int button, float mouseX, float mouseY){		// Called when mouse button released
	for (int i = 0; i < Buttons.size(); i++){
		Button* b = Buttons.at(i);
		if (b->isSelected() && b->getID() == 1 && button == 0){
			app->finishCurrentActivity(app->mainMenu);
		}
	}
}
void ControlActivity::onMouseMove(float mouseX, float mouseY){				// Called when mouse moved
	float x = ((mouseX - app->getScreenWidth() / 2) / (VIEW_SIZE));
	float y = ((app->getScreenHeight() / 2 - mouseY) / (VIEW_SIZE)) + 1;
	for each(Button* b in Buttons){
		b->checkForCollisions(x, y);
	}
}
void ControlActivity::onKeyDown(int key){										// Called when key pressed
	if (key == 0x27)
		rightKey = rightKeys.back();
	if (key == 0x25)
		leftKey = leftKeys.back();
	if (key == 68)
		dKey = dKeys.back();
	if (key == 65)
		aKey = aKeys.back();
	if (key == ' ')
		space = spaces.back();
	if (key == 80)
		pKey = pKeys.back();
}
void ControlActivity::onKeyUp(int key){										// Called when key released
	if (key == 0x27)
		rightKey = rightKeys.front();
	if (key == 0x25)
		leftKey = leftKeys.front();
	if (key == 68)
		dKey = dKeys.front();
	if (key == 65)
		aKey = aKeys.front();
	if (key == ' ')
		space = spaces.front();
	if (key = 80)
		pKey = pKeys.front();

}